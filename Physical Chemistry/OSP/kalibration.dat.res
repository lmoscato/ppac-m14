OSP Tropfenvolumen-Tensiometer
------------------------------
File                         : kalibration.dat 
OSP calibration 
4 Messungen:
1 1.994 82.4 41.40679
1 1.969 81.3 41.37274
1 1.993 82.1 41.27673
1 1.981 82.2 41.57735

Resultate (mit Standardabweichungen):
Kalibrationsfaktor [mm^3/mm] :   41.40840 (0.12537) 
Kapillarradius          [mm] :    2.99500 (0.01000) 
Dichte Umgebung    [mg/mm^3] :    0.00106 
Verschiebung            [mm] :    1.98425 (0.01176) 
Masse                   [mg] :   82.00000 (0.48305) 
Volumen               [mm^3] :   82.16386 (0.54598) 
Dichte             [mg/mm^3] :    0.99805 (0.00885) 
Parameter                    :    0.68893 (0.00276) 
Korrekturfaktor              :    0.60959 (0.00037) 
Oberflaechenspannung  [mN/m] :   70.02529 (0.45865) 
Monte Carlo Simulation mit 500000 Samples. 
Wed Apr 13 09:52:35 2022

1, 1.868, 77.2
1, 1.640, 69.1
1, 1.381, 61.9
1, 1.278, 57.4
1, 1.195, 53.1
1, 1.113, 51.0
1, 1.067, 49.1