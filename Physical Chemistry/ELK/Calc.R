#Experiment ELK
require(methods)
rm(list=ls())
#Versuch 1
#Idealer Tiefpass-Filter:
cat("Low Pass Filter:

")
C_cap_meas<-1.144*10^(-9)	#Verwendete Kapazität
R_res_meas<-10010 	#Verwendeter Wiederstand
Fun_Z_cap_ideal_calc<-function(w,C) { #Impedanz des idealen LPF
	return(0-(0+1i)/(w*C))
}
#U=Z*I -> U=(Z_C+Z_R)*I -> U=U_R+U_C -> U_C=U-U_R -> U_C=U-R*I
#Z=U/I -> I=U/Z -> U_C=U-R*U/Z_tot 
Fun_U_lpf_calc <-function(w,C,R,U) {	#Output-Spannung des RC-Filter
	U_lpf<-U-R*U/(Fun_Z_cap_ideal_calc(w,C)+R)
	return(U_lpf)
}
U_in<-10	#Verwendete Spannung
f_meas<-c(2540,3623,4100,5504,6517,7969,9204,10866,12082,13012,13564,14076,14519,15021,15637,18343,35225,49191,74715,97110,209688) #Verwendete Frequenzen Ausreisser:24814
U_out_meas<-c(10-0.4,10-0.4,10-0.4,10-0.8,10-1.2,8,7.8,7.4,7,6.8,6.6,6.6,6.2,6.2,6,5.4,3.3,2.5,1.7,1.3,3.1*0.2) #gemessene Output-Spannung	 Ausreisser:4.5
t_phasediff_meas<-10^(-6)*c(0.7*20,12,12,12,12,10,10,10,10,10,1.9*5,1.9*5,1.8*5,1.8*5,1.8*5,1.7*5,2.8*2,4.3,3.0,4.7*0.5,5.7*0.2) #gemessene Phasenverschiebung in Sekunden Ausreisser:,2.8*2
G_lpf_calc<-U_out_meas/U_in #Spannungsgang
G_lpf_ideal_calc_seq<-abs(Fun_U_lpf_calc(2*pi*2*10^seq(3,6,0.05),C_cap_meas,R_res_meas,U_in))/U_in #Spannungsgang, idealisiert
print("Regression for low-pass filter:")
model_G<-nls(G_lpf_calc~a*abs(Fun_U_lpf_calc(2*pi*f_meas,C,R_res_meas,U_in))/U_in, trace=TRUE, start=list(a=1,C=C_cap_meas))
print(summary(model_G))
#Phasengang
phi_lpf_calc_degree=-t_phasediff_meas*f_meas*360 #Phasenverschiebung in Grad
phi_lpf_calc_ideal_seq<-(180/pi)*atan(Im(Fun_U_lpf_calc(2*pi*2*10^seq(3,6,0.05),C_cap_meas,R_res_meas,U_in))/Re(Fun_U_lpf_calc(2*pi*2*10^seq(3,6,0.05),C_cap_meas,R_res_meas,U_in))) #Phasenverschiebung idealisiert
model_phi<-nls(phi_lpf_calc_degree~a*atan(Im(Fun_U_lpf_calc(2*pi*f_meas,C,R_res_meas,U_in))/Re(Fun_U_lpf_calc(2*pi*f_meas,C,R_res_meas,U_in))),trace=TRUE,start=list(a=180/pi, C=C_cap_meas))
print(summary(model_phi))
#3dB-Freq
C_reg<-coefficients(model_G)[2]
C_reg_std_error<-summary(model_G)$coefficients[2,2]
f_lpf_grenz<-1/(2*pi*R_res_meas*C_reg)
f_lpf_grenz_error_final=abs((1/(2*pi*C_reg^2*R_res_meas))*C_reg_std_error/sqrt(length(G_lpf_calc)))*qt(0.975,df=19)
f_lpf_grenz_tan<-1/(2*pi*R_res_meas*coefficients(model_phi)[2])
f_lpf_grenz_tan_error<-abs((1/(2*pi*(coefficients(model_phi)[2])^2*R_res_meas))*summary(model_phi)$coefficients[2,2])
cat(c("
","C=",C_reg,"±",C_reg_std_error*qt(0.975,df=19)/sqrt(length(G_lpf_calc)),"

cutoff frequency: ",f_lpf_grenz,"±", f_lpf_grenz_error_final,"
(using Voltage data set
Using Frequency data set: ",f_lpf_grenz_tan,"±",f_lpf_grenz_tan_error*qt(0.975,df=19)/sqrt(length(phi_lpf_calc_degree)),")

"))


#confidence for plot 1
#plotting
plot(f_meas,G_lpf_calc,type="p",log="x",xlab="Frequency [Hz]",ylab="Relative voltage change") #Ausreisser:0.450
points(2*10^seq(3,6,0.05),G_lpf_ideal_calc_seq,type="l",lty=2)
points(2*10^seq(3,6,0.05),predict(model_G, list(f_meas=2*10^seq(3,6,0.05))),type="l")
#points(2*10^seq(3,6,0.05),predict(model_G, list(f_meas=2*10^seq(3,6,0.05)),interval="confidence",level=0.95),type="l") #HAHAHA sehr witzig R project! WARUM IST DAS ÜBERHAUPT IMPLEMENTIERT, WENN ES ZUR ZEIT EH NICHT FUNKTIOBIERT!!!!!
points(24814,0.45,type="p") #Ausreisser
abline(v=f_lpf_grenz, lty=3)
abline(h=1/sqrt(2),lty=3)
dev.copy2pdf(file="Plot_1_Phasengang_calc_meas_ideal.pdf")
dev.off()
plot(f_meas,phi_lpf_calc_degree,log="x",type="p",ylim=c(-95,1),xlab="Frequency [Hz]",ylab="Phase shift [°]") #Ausreisser: -50.02502
points(2*10^seq(3,6,0.05),phi_lpf_calc_ideal_seq,type="l",lty=2)
points(2*10^seq(3,6,0.05),predict(model_phi, list(f_meas=2*10^seq(3,6,0.05))), type="l")
points(24814,-50.025,type="p") #Ausreisser
abline(v=f_lpf_grenz, lty=2)
abline(h=-45, lty=2)
dev.copy2pdf(file="Plot_2_Phasenwinkel_calc_meas.pdf")
dev.off()
plot(log(f_meas, base=10)-log(f_lpf_grenz,base=10),log(G_lpf_calc, base=10)*20,type="p",log="",xlab="log(x)",ylab="Relative voltage change [dB]") #Ausreisser:0.450
points(log(2*10^seq(3,6,0.05), base=10)-log(f_lpf_grenz, base=10),log(G_lpf_ideal_calc_seq, base=10)*20,type="l",lty=2)
points(log(2*10^seq(3,6,0.05),base=10)-log(f_lpf_grenz, base=10),log(predict(model_G, list(f_meas=2*10^seq(3,6,0.05))), base=10)*20,type="l")
points(0.3154,-15.97,type="p") #Ausreisser
abline(v=0, lty=2)
abline(h=-3, lty=2)
dev.copy2pdf(file="Plot_3.1_Bode_Diagram_for_lpf_G.pdf")
dev.off()
plot(log(f_meas, base=10)-log(f_lpf_grenz, base=10),phi_lpf_calc_degree,log="",type="p",ylim=c(-95,1),xlab="log(x)",ylab="Phase shift [°]") #Ausreisser: -50.02502
points(log(2*10^seq(3,6,0.05),base=10)-log(f_lpf_grenz,base=10),phi_lpf_calc_ideal_seq,type="l",lty=2)
points(log(2*10^seq(3,6,0.05),base=10)-log(f_lpf_grenz,base=10),predict(model_phi, list(f_meas=2*10^seq(3,6,0.05))), type="l")
points(0.3154,-50.025,type="p") #Ausreisser
abline(v=0, lty=2)
abline(h=-45, lty=2)
dev.copy2pdf(file="Plot_3.2_Bode_Diagram_for_lpf_phase.pdf")
dev.off()

#Versuch 2
rm(list=ls())
cat("
Band pass filter:

")
#Schwingkreis
C_cap_meas=105.3*10^(-9)
R_res_meas=1001
f_meas=c(1247,3866,5385,6239,7004,7729,8178,8570,8834,9641,10035,10773,11350,11965,12975,14162,15737,16949,18248,37630,106494)
U_in=10
U_out_meas=c(3.7*0.05,0.6,1.4,1.4,1.85,2.6,3.2,4.2,4.8,5.8,5.8,5.0,4.2,3.6,2.75,2.15,1.6,1.4,1.2,0.43,2.7*0.05)
t_shift_meas=10^(-6)*c(-3.6*50,-56,-41,-33,-27,-23,-18,-15,-9,0.06,4.4,10,12,14,15,15,13,13,13,6.4,2.2)
phi_shift_calc=t_shift_meas*f_meas*2*pi
G_calc=U_out_meas/U_in
QP<-function(w,R,L,C) {
	Z_C<-(0-1i)/(w*C)
	Z_L<-(0+1i)*w*L
	ZE<-R+1/(1/Z_C + 1/Z_L)
	ZA<-1/(1/Z_C+1/Z_L)
	G<-ZA/ZE
	return(G)
}
print("Regression for bandpass filter:")
model_G<-nls(G_calc~a*abs(QP(2*pi*f_meas,R_res_meas,L,C)), trace=TRUE, start=list(a=1,C=C_cap_meas,L=0.02))
print(summary(model_G))
L_reg_model_G<-coefficients(model_G)[3]
C_reg_model_G<-coefficients(model_G)[2]
C_reg_model_G_std_error<-summary(model_G)$coefficients[2,2]
L_reg_model_G_std_error<-summary(model_G)$coefficients[3,2]
cat(c("
","L=",L_reg_model_G,"±",L_reg_model_G_std_error*qt(0.975,df=18)/sqrt(length(G_calc)),"
"))
cat(c("
","C=",C_reg_model_G,"±",C_reg_model_G_std_error*qt(0.975,df=18)/sqrt(length(G_calc)),"

"))
QP_Phi<-function(w,R,L,C) {
	return(atan(Im(QP(w,R,L,C))/Re(QP(w,R,L,C))))
}
model_phi<-nls(phi_shift_calc~a*QP_Phi(2*pi*f_meas,R_res_meas,L,coefficients(model_G)[2]), trace=TRUE, start=list(a=1, L=coefficients(model_G)[3]))
print(summary(model_phi))
#Resonator-Güte
#>AAAAAAAAAAAAAAAAAAAAAAAAJHHHHHHHHHHHHHHHHHHHHHHHHHHhesrktuzfziuo
Fun_Grenz_BPF<-function(R,C,L,Cer,Ler,n) {
	f_low<-(-1)				#TEMP
	f_up<-1
	f_res<-1/(2*pi*sqrt(L*C))
	Q<-f_res/(f_up-f_low)
	f_res_std_error_mean<-sqrt(Ler^2/(n*16*pi^2*C*L^3)+Cer^2/(n*16*pi^2*L*C^3))				#Error propagation on 1/(2*pi*sqrt(L*C)) using the derivatives in respect to C and L
	f_res_error_final<-f_res_std_error_mean*qt(0.975, df=18)
	return(c(f_res,f_res_error_final))
}
Q_set=Fun_Grenz_BPF(R_res_meas,C_reg_model_G,L_reg_model_G,C_reg_model_G_std_error,L_reg_model_G_std_error,length(G_calc))
cat(c("Resonance frequency: ",Q_set[1], "±", Q_set[2]), "
")
cat("
manual approach to finding f_3dB:
")
print(log(predict(model_G, list(f_meas=seq(5e03,1e04,1e02), a=1)), base=10)*20)
print(seq(5e03,1e04,1e02))
print(log(predict(model_G, list(f_meas=seq(8600,8700,1), a=1)), base=10)*20)
print(seq(8600,8700,1))
f_3db_approx_lower<-8671
cat(c("
",
"Approximate f_3dB lower: ", f_3db_approx_lower,
"
"))
print(log(predict(model_G, list(f_meas=seq(1e04,2e04,100), a=1)), base=10)*20)
print(seq(1e04,2e04,100))
print(log(predict(model_G, list(f_meas=seq(11300,11400,1), a=1)), base=10)*20)
print(seq(11300,11400,1))
f_3db_approx_upper<-11347
cat(c("
",
"Approximate f_3dB upper: ", f_3db_approx_upper,
"
"))
cat(c("Q=",Q_set[1]/(f_3db_approx_upper-f_3db_approx_lower),"
"))
#plotting
plot(f_meas,G_calc,log="x",type="p",ylim=c(0,1), xlab="Frequency[Hz]", ylab="Relative voltage change")
points(10^seq(3,5.2,0.05),predict(model_G, list(f_meas=10^seq(3,5.2,0.05))),type="l")
abline(h=predict(model_G, list(f_meas=Q_set[1]))/sqrt(2), lty=2)
abline(h=predict(model_G, list(f_meas=Q_set[1])), lty=2)
abline(v=Q_set[1], lty=2)
abline(v=f_3db_approx_lower, lty=2)
abline(v=f_3db_approx_upper, lty=2)
dev.copy2pdf(file="Plot_4_Phasengang_calc_meas.pdf")
dev.off()
plot(f_meas,180*phi_shift_calc/pi, log="x", ylab="Phase Shift [°]", xlab="Frequency measured [Hz]")
points(f_meas,predict(model_phi)*180/pi,type="l")
abline(h=0, lty=2)
abline(v=Q_set[1], lty=2)
dev.copy2pdf(file="Plot_5_Phaseshift_meas.pdf")
dev.off()
plot(log(f_meas, base=10)-log(Q_set[1], base=10),log(G_calc, base=10)*20,log="",type="p",ylim=c(-41,0), xlab="log(x)", ylab="Relative voltage change [dB]")
points(log(10^seq(3,5.2,0.05), base=10)-log(Q_set[1], base=10),log(predict(model_G, list(f_meas=10^seq(3,5.2,0.05))), base=10)*20,type="l")
abline(v=0, lty=2)
abline(lty=2, h=log(predict(model_G, list(f_meas=Q_set[1])), base=10)*20)
abline(lty=2, h=log(predict(model_G, list(f_meas=Q_set[1])), base=10)*20-3)
abline(v=log(f_3db_approx_lower, base=10)-log(Q_set[1],base=10), lty=2)
abline(v=log(f_3db_approx_upper, base=10)-log(Q_set[1],base=10), lty=2)
dev.copy2pdf(file="Plot_6.1_Bode_diagramm_for_resonator_G.pdf")
dev.off()
plot(log(f_meas, base=10)-log(Q_set[1], base=10),180*phi_shift_calc/pi, log="", type="p", xlab="log(x)", ylab="Phase shift [°]")
points(log(f_meas, base=10)-log(Q_set[1], base=10),predict(model_phi)*180/pi,type="l")
abline(h=0, lty=2)
abline(v=0, lty=2)
dev.copy2pdf(file="Plot_6.2_Bode_diagramm_for_resonator_phase.pdf")
dev.off()

#Versuch 3
rm(list=ls())
cat("

Dampened Square-wave

")
#Rechteckwellen gedämpfte Oszillation
max_meas=c(1.4,0.7,0.4,0.24,0.14)
t_meas_max=0.04+c(-0.01,0.09,0.18,0.28,0.38)
min_meas=c(-0.9,-0.5,-0.3,-0.16)
t_meas_min=0.04+c(0.04,0.14,0.23,0.33)
#y~A*exp(-x) -> ln(y)~ln(A)-x 
model_max <- lm(log(max_meas)~t_meas_max)
print(summary(model_max))
model_min <- lm(log(-min_meas)~t_meas_min)
print(summary(model_min))
model_sine<-lm(c(t_meas_max, t_meas_min)~c(1,3,5,7,9,2,4,6,8)) #correction factor for time shifts
print(summary(model_sine))
plot(c(t_meas_max, t_meas_min),c(max_meas,min_meas),ylim=c(-1.5,1.5),xlim=c(0.015,0.42),ylab="Voltage output [V]", xlab="time difference [ms]")
points(seq(0,0.5,0.005),exp(coefficients(model_max)[1]+coefficients(model_max)[2]*seq(0,0.5,0.005)),type="l",lty=2)
points(seq(0,0.5,0.005),-exp(coefficients(model_min)[1]+coefficients(model_min)[2]*seq(0,0.5,0.005)),type="l",lty=2)
points(seq(0,0.5,0.001), exp((coefficients(model_max)[1]+coefficients(model_min)[1])/2 + (coefficients(model_max)[2]+coefficients(model_min)[2])*seq(0,0.5,0.001)/2)*sin(seq(0,0.5,0.001)*pi/coefficients(model_sine)[2]),type="l")
dev.copy2pdf(file="Plot_7_Squarewave_decay_meas_calc.pdf")
dev.off()
cat("

")
cat(c("tau_dampening=",-(coefficients(model_max)[2]+coefficients(model_min)[2])/2,"±",sqrt((summary(model_min)$coefficients[2,2])^2+(summary(model_max)$coefficients[2,2])^2)*qt(0.975, df=2)/sqrt(4),"ms^(-1)","
"))


#endlich, das Resultat 20 langer Stunden!
