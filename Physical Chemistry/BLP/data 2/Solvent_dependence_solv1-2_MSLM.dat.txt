
Datei: Solvent_dependence_solv1-2_MSLM.dat
Fit function:  a + b * exp(-c * t)
t-Intervall: 27.500 ... 1975.000 ms
Parameters (Standard deviation):
  a = -0.00344 (0.00020)
  b = 0.29747 (0.00035)
  c = 2.021e+00 (5.764e-03) s^-1
  tau = 4.948e-01 (1.411e-03) s
