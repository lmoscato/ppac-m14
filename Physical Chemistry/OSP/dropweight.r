
# SURFACE TENSION-----------------------------------------------

rm(list = ls())
library(extrafont)
options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
windows(width = 8, height = 6)
par(family = "CM Roman")

data <- read.table("ospdata.dat", header = TRUE)
lit_data <- read.table("litdata_dmsowater.dat")

x_dmso_lit <- lit_data[, 1]
surftens_lit <- lit_data[, 2]
red_surftens_lit <- (max(surftens_lit) - surftens_lit) / (max(surftens_lit) - min(surftens_lit)) #nolint

x_dmso <- data[, 1]
density <- data[, 2]
density_sd <- data[, 3]
surftens <- data[, 4]
surftens_sd <- data[, 5]



red_surftens <- (max(surftens) - surftens) / (max(surftens) - min(surftens))

x <- seq(0, 1, length = 1000)
y <- seq(max(surftens), min(surftens), length = 1000)

surftens_conf <- surftens_sd * qt(0.975, df = 2)
model <- lm(surftens ~ poly(x_dmso, 2, raw = TRUE))
prediction <- predict(model, newdata = data.frame(x_dmso = seq(0, 1, length = 1000)), interval = "confidence") #nolint

plot(x_dmso, surftens,
    xlab = expression(italic(x)[DMSO]),
    ylab = expression("Surface Tension" ~ "/" ~ mNm^-1),
    las = 1,
    pch = 21
)
lines(x, prediction[, 1])
lines(x, y,
    col = "grey",
    lty = 2
)
points(x_dmso_lit, surftens_lit,
    pch = 22,
    bg = "black",
    cex = 0.5
)

legend("topright",
    legend = c("Ideal line", "Fitted curve", "Data points", "Literature values"), #nolint
    lty = c(2, 1, NA, NA),
    pch = c(NA, NA, 21, 22),
    pt.bg = c(NA, NA, "white", "black"),
    col = c("grey", "black", "black", "black"),
    pt.cex = c(NA, NA, 1, 0.5)
)

dev.copy2pdf(file = "dropweight_dmso.pdf", width = 8, height = 6)
dev.off()

#REDUCED SURFACE TENSION-----------------------------------------------

windows(width = 8, height = 6)
par(family = "CM Roman")

model2 <- lm(red_surftens ~ poly(x_dmso, 2, raw = TRUE))
prediction <- predict(model2, newdata = data.frame(x_dmso = x))

plot(x_dmso, red_surftens,
    xlab = expression(italic(x)[DMSO]),
    ylab = expression("Reduced Surface Tension"),
    las = 1
)
lines(x, prediction)
lines(x, x,
    lty = 2,
    col = "grey"
)
points(
    x_dmso_lit, red_surftens_lit,
    pch = 22,
    bg = "black",
    cex = 0.5
)

legend("bottomright",
    legend = c("Ideal line", "Fitted curve", "Data points", "Literature Values"), #nolint
    lty = c(2, 1, NA, NA),
    pch = c(NA, NA, 21, 22),
    pt.bg = c(NA, NA, "white", "black"),
    col = c("grey", "black", "black", "black"),
    pt.cex = c(NA, NA, 1, 0.5)
)

dev.copy2pdf(file = "dropweight_dmso_reduced.pdf", width = 8, height = 6)
dev.off()

#DENSITY--------------------------------------------------

windows(width = 8, height = 6)
par(family = "CM Roman")

y_density <- seq(min(density), max(density), length = 1000)

plot(
    x_dmso, density,
    xlab = expression(italic(x)[DMSO]),
    ylab = expression("Density" ~ "/" ~ g ~ cm^-3),
    ylim = c(0.9, 1.2),
    pch = 21,
    las = 1
)

lines(x, y_density,
    col = "gray",
    lty = 2
)

legend("topright",
    legend = c("Data points", "Ideal line"),
    lty = c(NA, 2),
    col = c("black", "gray"),
    pch = c(21, NA)
)

dev.copy2pdf(file = "dropweight_density.pdf", width = 8, height = 6)
dev.off()
