rm(list = ls())
source(file = "minorticks.r")
source("find_confint.r")
windows(height = 6, width = 8)
library(errors)
library(extrafont)
options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
options(error.notation = "plus-minus", errors.digits = 2)
par(family = "CM Roman")

# VARIABLES DECLARATION ------------------------------------
r <- 10.02e3
c <- 1.010e-9
logf <- seq(1, 6, length = 500)
f <- 10^logf
w <- 2 * pi * f

# VIERPOL FUNCTION -----------------------------------------
vierpol <- function(r, w, c) {  # nolint
zr <- r
zc <- (0 + 1i) / (- w * c)
z_total <- zr + zc
g <- zc / z_total
abs(g)
}

# EXPERIMENTAL DATA ----------------------------------------
f_exp <- 1000 * c(1.04051, 5.12067, 10.3592, 15.12861, 20.2186, 25.0169,
                  30.6514, 35.5868, 40.15583, 45.02093, 50.7181, 55.7208,
                  75.00523, 100.3161, 2.02874, 3.54517, 4.0099, 12.2911,
                  13.1281, 16.34479)

entrance_voltage <- 10

exit_voltage_in_divisions <- c(5.0, 4.5, 7.6, 6.4, 5.3,
                               4.6, 8.0, 7.0, 6.4, 5.8,
                               5.1, 4.8, 3.6, 6.8, 4.8,
                               4.6, 4.6, 6.2, 7.0, 6.3)
voltage_divisions_values <- c(2, 2, 1, 1, 1,
                              1, 0.5, 0.5, 0.5, 0.5,
                              0.5, 0.5, 0.5, 0.2, 2,
                              2, 2, 1, 1, 1)

exit_voltages <- exit_voltage_in_divisions * voltage_divisions_values
w_exp <- 2 * pi * f_exp

g_exp <- exit_voltages / entrance_voltage

# FITTING --------------------------------------------------

fit <- nls(g_exp ~ a * vierpol(r, w_exp, C),
           start = list(a = 1, C = 1e-9),
           control = nls.control(maxiter = 1000))

print(summary(fit))
g_c <- predict(fit, list(w_exp = w))

c_fit <- coef(fit)[2]
errors(c_fit) <- summary(fit)$coefficients[2, 2]

cutoff_freq_fit <- 1 / (2 * pi * r * c_fit)
errors(cutoff_freq_fit) <- find_confint(cutoff_freq_fit, 18)

# PLOTTING -------------------------------------------------
plot(log10(f), g_c,
     type = "l",
     xlim = c(1, 6),
     ylim = c(0, 1),
     xlab = expression(italic("f") ~ " [Hz]"),
     xaxt = "n",
     ylab = expression("|G(" ~ italic("f") ~ ")|"),
     col = "red",
     las = 1)

lines(log10(f), vierpol(r, w, c), col = "#005d00")

points(log10(f_exp), g_exp, pch = 21, bg = "blue")

minor.ticks.axis(1, 15, mn = 0, mx = 15)

abline(h = 1 / sqrt(2) * max(g_c), lty = 3, lwd = 1)
abline(v = log10(cutoff_freq_fit), lty = 3, lwd = 1)


legend("topright",
      legend = c("Theoretical curve", "Fitted curve", "Data points"),
      lty = c(1, 1, NA),
      lwd = c(2, 2, NA),
      pch = c(NA, NA, 21),
      col = c("#005d00", "red", "blue"),
      pt.bg = "blue",
      plot = TRUE,
      cex = 1,
      pt.cex = 1.5
)

cat("Cutoff frequency [Hz]: ")
print(cutoff_freq_fit)

dev.copy2pdf(file = "lowpassfilter_amplitude.pdf", width = 8, height = 6)