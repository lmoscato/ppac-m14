rm(list = ls())

P <- 0.958

library(extrafont)
library(errors)
library(fontcm)
source("errbars.R")
source("functions.r")

options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
options(error.notation = "plus-minus", errors.digits = 2)
windows(width = 8, height = 6)
par(family = "CM Roman")


mm_hexane <- 86.18
mm_toluene <- 92.14

antoine_hexane <- c(4.00266, 1171.53, -48.784)
antoine_toluene <- c(4.07827, 1343.943, -53.773)

masses_hexane <- c(1.01, 0.92, 0.79, 0.71, 0.62,
                   0.51, 0.41, 0.30, 0.21, 0.09, 0
)

masses_toluene <- c(0, 0.11, 0.23, 0.31, 0.42,
                    0.52, 0.65, 0.73, 0.84, 0.92, 1.00
)

refractive_indexes_cal <- 1 + 1e-5 * c(37505, 38565, 39819, 40573, 41626, 42772,
                                 44153, 45808, 46588, 48226, 49674
)


temperatures <- 1 / 10 * c(665, 676, 687, 702, 712, 723, 731, 739,
                           791, 802, 818, 837, 865, 901, 972, 1079
)

refractive_indexes_liq <- 1 + 1e-5 * c(37519, 38153, 39144, 39980,
                                       40526, 41124, 41604, 42137,
                                       44602, 44697, 45653, 46404,
                                       47090, 47888, 48797, 49636
)

refractive_indexes_gas <- 1 + 1e-5 * c(37515, 37583, 37758, 38003,
                                       38124, 38303, 38394, 38520,
                                       39252, 40692, 39782, 40044,
                                       40629, 41790, 44076, 48462
)


moles_hexane <- masses_hexane / mm_hexane
moles_toluene <- masses_toluene / mm_toluene
x1 <- seq(0, 1, length = 200)

hexane_mole_fraction <- moles_hexane / (moles_toluene + moles_hexane)
toluene_mole_fraction <- 1 - hexane_mole_fraction

model <- lm(refractive_indexes_cal ~ poly(hexane_mole_fraction, 2, raw = TRUE) #nolint
)

prediction <- predict(model,
                      interval = "confidence",
                      newdata = data.frame(hexane_mole_fraction = seq(0, 1, length = 200)) #nolint
)

plot(hexane_mole_fraction, refractive_indexes_cal,
    pch = 21,
    cex = 1.1,
    bg = "grey",
    xlab = expression(x[1]),
    ylab = expression(n[D]^20)
)
lines(x1, prediction[, 1])
lines(x1, prediction[, 2], col = "red", lty = 2)
lines(x1, prediction[, 3], col = "red", lty = 2)

approx_dat_liq <- approx(prediction[, 1], x1, refractive_indexes_liq, rule = 2)$y #nolint
approx_lwr_liq <- approx(prediction[, 2], x1, refractive_indexes_liq, rule = 2)$y #nolint
approx_upr_liq <- approx(prediction[, 3], x1, refractive_indexes_liq, rule = 2)$y #nolint

approx_dat_gas <- approx(prediction[, 1], x1, refractive_indexes_gas, rule = 2)$y #nolint
approx_lwr_gas <- approx(prediction[, 2], x1, refractive_indexes_gas, rule = 2)$y #nolint
approx_upr_gas <- approx(prediction[, 3], x1, refractive_indexes_gas, rule = 2)$y #nolint

dev.copy2pdf(file = "calibration.pdf", width = 8, height = 6)

dev.off()
windows(width = 8, height = 6)
par(family = "CM Roman")

plot(approx_dat_liq, approx_dat_gas,
    pch = 21,
    bg = "grey",
    xlab = expression(x[1]^"(l)"),
    ylab = expression(x[1]^"(g)"),
    ylim = c(0, 1)
)

lines(x1, x1, lty = 3)

arrows(approx_lwr_liq, approx_dat_gas, approx_upr_liq,
        code = 3, angle = 90,
        length = 0.05
)

arrows(approx_dat_liq, approx_lwr_gas,
        y1 = approx_upr_gas,
        code = 3, angle = 90,
        length  = 0.05
)

pv <- antoine(temperatures, antoine_hexane)



model2 <- nls(approx_dat_gas ~ raoult1(approx_dat_liq, pv, L12, L21),
            start = list(L12 = 0.5, L21 = 0.59841)
)
L12 <- coef(model2)[1] #nolint
L12_error <- summary(model2)$coefficients[1, 2]
L12_error <- L12_error * qt(0.975, df = 16)
L21 <- coef(model2)[2] #nolint
L21_error <- summary(model2)$coefficients[2, 2]
L21_error <- L21_error * qt(0.975, df = 16)
t <- t_ideal <- numeric(length = 200)

for(i in 1:200) {
   t[i] <- uniroot(rootfinding, c(50, 150), x1[i], L12, L21)$root
   t_ideal[i] <- uniroot(rootfinding, c(50, 150), x1[i], 1, 1)$root
}

y1 <- raoult1(x1, antoine(t, antoine_hexane), L12, L21)
y_ideal <- raoult1(x1, antoine(t_ideal, antoine_hexane), 1, 1)
lines(x1, y1)
lines(x1, y_ideal, lty = 2)

legend("bottomright",
    legend = c("Real", "Ideal", expression(italic(x)[1]^"(l)" ~ "=" ~ italic(x)[1]^"(g)")), #nolint
    lty = c(1, 2, 3)

)





dev.copy2pdf(file = "VLE.pdf", width = 8, height = 6)

dev.off()
windows(width = 8, height = 6)
par(family = "CM Roman")

plot(x1, t, type = "l",
    xlab = expression(italic(x)[1]^"(l)/(g)"),
    ylab = expression("Temperature" ~ "/" ~ "°C")
)
lines(y1, t)
lines(y_ideal, t_ideal, lty = 2)
lines(x1, t_ideal, lty = 2)

legend("topright",
    legend = c("Ideal", "Real"),
    lty = c(2, 1)
)

points(approx_dat_liq, temperatures, pch = 21, col = "blue", bg = "grey")
arrows(approx_lwr_liq, temperatures, approx_upr_liq,
        code = 3, angle = 90, col = "blue", length = 0.05
)

points(approx_dat_gas, temperatures, pch = 21, col = "red", bg = "grey")
arrows(approx_lwr_gas, temperatures, approx_upr_gas,
        code = 3, angle = 90, length = 0.05, col = "red"
)


dev.copy2pdf(file = "SDG.pdf", width = 8, height = 6)
dev.off()

windows(width = 8, height = 6)
par(family = "CM Roman")

plot(x1, y1 * P,
    type = "l",
    ylim = c(0, 1),
    xlab = expression(x[1]^"(l)"),
    ylab = "P / bar",
)

lines(x1, y_ideal * P,
    lty = 2,
)

lines(x1, (1 - y1) * P,
    col = "blue"
)

lines(x1, (1 - y_ideal) * P,
    lty = 2,
    col = "blue"
)

points(approx_dat_liq, approx_dat_gas * P,
    pch = 21, bg = "grey", cex = 1.1
)
arrows(approx_lwr_liq, approx_dat_gas * P, approx_upr_liq,
    code = 3, angle = 90, length = 0.05, col = "black"
)

segments(0, 0.958, 1, lwd = 2, col = "red")
text(0.5, 1, "Ambient pressure = 0.958 bar")

legend("right",
    legend = c("Hexane (real)", "Hexane (ideal)", "Toluene (real)", "Toluene (ideal)"), #nolint
    lty = c(1, 2, 1, 2),
    col = c("black", "black", "blue", "blue"),
    lwd = c(2, 2, 2, 2)
)
dev.copy2pdf(file = "DDR.pdf", width = 8, height = 6)
dev.off()