rm(list = ls()) #removing all variables
library(extrafont) # package to change font
source("../minorticks.r")

# DATA-----------------------------------------
cacl2_molarmass <- cacl2_molarmass <- 147.02 # grams per mole

stock1_mass <- 0.7480 # g
stock2_mass <- 0.0714 # g

stock_volume <- 0.050 # L

stock1_mole <- stock1_mass / cacl2_molarmass # mol
stock2_mole <- stock2_mass / cacl2_molarmass # mol

stock1_conc <- stock1_mole / stock_volume # g per mole
stock2_conc <- stock2_mole / stock_volume # g per mole

calib6_conc <- stock2_conc * 10e-4
calib5_conc <- stock2_conc * 10e-3
calib4_conc <- stock2_conc * 10e-2
calib3_conc <- stock2_conc * 10e-1
calib2_conc <- stock1_conc * 10e-1

concentrations <- c(
    calib6_conc,
    calib5_conc,
    calib4_conc,
    calib3_conc,
    calib2_conc
)
conc_log <- log10(concentrations)

potentials <- -1 * c(219.6, 216.3, 189.9, 165.2, 135.4)
xaxis <- seq(-5, -0.5, length = 1000) #xaxis seq. for prediction

#FITTING---------------------------------------
range <- c(1, 3:5) #to exclude bad point from fit
conc_log_fit <- conc_log[range]
calib_fit <- lm(potentials[range] ~ conc_log_fit) #linear fit of data
calib_pred <- predict(calib_fit, # predict based on model with conf. interval
    interval = "confidence",
    newdata <- data.frame(conc_log_fit = xaxis)
)


# PLOTTING------------------------------------

windows(width = 8, height = 6) #WINDOWS LINE
#x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman")

plot(conc_log, potentials,
    xlab = expression(c[Ca] ~ "[mol/L]"),
    ylab = "U [mV]"
)

lines(xaxis, calib_pred[, 1])

lines(xaxis, calib_pred[, 2],
    lty = 2,
    col = "red"
)

lines(xaxis, calib_pred[, 3],
    lty = 2,
    col = "red"
)