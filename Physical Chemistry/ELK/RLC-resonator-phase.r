rm(list = ls())
source(file = "minorticks.r")
source("find_confint.r")
library(extrafont)
library(errors)
# loadfonts() nolint
 windows(width = 8, height = 6)
options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
options(errors.notation = "plus-minus")
par(family = "CM Roman")

# VIERPOL FUNCTION------------------------------------------
vierpol <- function(w, r, l, c) {
    zr <- r
    zc <- (0 + 1i) / (-w * c)
    zl <- (0 + 1i) * (w * l)
    ze <- zr + 1 / (1 / zc + 1 / zl)
    za <- 1 / (1 / zc + 1 / zl)
    g <- za / ze
    g
}

# VARIABLES DECLARATION-------------------------------------
r <- 1.001e3
L <- 2.32e-6 * 30^2 # nolint
cap <- 104.4e-9
entrance_voltage <- 10
theoretical_resonance_freq <- 1 / (2 * pi * sqrt(L * cap))
theoretical_resonance_freq <- log10(theoretical_resonance_freq)

logf <- seq(1, 6, length = 1000)
f <- 10^logf
w <- 2 * pi * f
phi <- Arg(vierpol(w, r, L, cap)) * (180 / pi)

# EXPERIMENTAL DATA-----------------------------------------

f_exp <- 1e3 * c(1.0513, 2.04636, 3.49041, 4.37654, 8.44402,
                 9.2987, 10.0643, 10.70391, 11.27612, 12.55891,
                 13.51844, 14.29113, 20.25346, 28.65474, 34.4407,
                 42.25312, 7.18536, 8.15735, 6.58913, 10.31054)
w_exp <- 2 * pi * f_exp

phaseshift_in_divisions <- c(2.5, 2.3, 3.2, 2.6, 1.5,
                             0.4, 0.5, -1.8, -2.3, -1.5,
                             -2.8, -1.3, -2.2, -1.7, -1.4,
                             -2.8, 2.6, 1.8, 3.1, 0.6)

time_per_division <- 1e-6 * c(100, 50, 20, 20, 10,
                              10, 10, 5, 5, 10,
                              5, 10, 5, 5, 5,
                              2, 10, 10, 10, 10)

phaseshift_time <- phaseshift_in_divisions * time_per_division
phaseshift <- phaseshift_time * f_exp * 2 * pi
phaseshift <- phaseshift * (180 / pi)


# FITTING---------------------------------------------------

model <- nls(phaseshift ~ a * Arg(vierpol(w_exp, R, L, c)),
             start = list(a = 1, R = 1000, c = 100e-9),
             control = nls.control(maxiter = 1000)
            )

print(summary(model))

phi_fit <- predict(model, list(w_exp = w))

c_fit <- coef(model)[3]
errors(c_fit) <- summary(model)$coefficients[3, 2]

f_res_fit <- 1 / (2 * pi * sqrt(L * c_fit))
errors(f_res_fit) <- find_confint(f_res_fit, 17)

# PLOTTING--------------------------------------------------

plot(log10(f_exp), phaseshift,
     type = "p",
     pch = 21,
     bg = "blue",
     xaxt = "n",
     yaxt = "n",
     xlab = expression(italic("f") ~ "[Hz]"),
     ylab = "Phase shift [degrees]"
)
minor.ticks.axis(1, 8, mn = 0, mx = 7)
axis(2, at = seq(-90, 90, 30), las = 1)

rug(x = c(seq(-90, 90, 30) + 10, seq(-90, 90, 30) + 20),
    ticksize = -0.01, side = 2)

lines(logf, phi, col = "#005d008b")

lines(logf, phi_fit, col = "red")

abline(v = log10(f_res_fit), lty = 3)
abline(h = 0, lty = 3)

legend("topright",
      legend = c("Theoretical curve", "Fitted curve", "Data points"),
      lty = c(1, 1, NA),
      lwd = c(2, 2, NA),
      pch = c(NA, NA, 21),
      col = c("#005d008b", "red", "black"),
      pt.bg = "blue",
      plot = TRUE,
      cex = 1,
      pt.cex = 1.5
     )

cat("Resonance Frequency [Hz]: ")
print(f_res_fit)

dev.copy2pdf(file = "RLC-resonator-phase.pdf", width = 8, height = 6)
dev.off()