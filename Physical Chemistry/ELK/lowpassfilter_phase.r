rm(list = ls())
source(file = "minorticks.r")
source("find_confint.r")
library(errors)
library(extrafont)
options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
options(error.notation = "plus-minus", errors.digits = 2)
par(family = "CM Roman")

# VIERPOL FUNCTION------------------------------------------
vierpol <- function(r, w, c) {  # nolint
zr <- r
zc <- (0 + 1i) / (- w * c)
z_total <- zr + zc
g <- zc / z_total
return(g)
}

# VARIABLES DECLARATION-------------------------------------
r <- 10.02e3
c <- 1.010e-9
logf <- seq(1, 7, length = 500)
f <- 10^logf
w <- 2 * pi * f
cutoff_freq <- log10(1 / (2 * pi * r * c))

abs_g <- abs(vierpol(r, w, c))
phi <- Arg(vierpol(r, w, c))
phi <- phi * (180 / pi)

# EXPERIMENTAL DATA ----------------------------------------
f_exp <- 1000 * c(1.04051, 5.12067, 10.3592, 15.12861, 20.2186, 25.0169,
                  30.6514, 35.5868, 40.15583, 45.02093, 50.7181, 55.7208,
                  75.00523, 100.3161, 2.02874, 3.54517, 4.0099, 12.2911,
                  13.1281, 16.34479
)
w_exp <- f_exp * 2 * pi

entrance_voltage <- 10

phaseshift_in_divisions <-  c(0.1, 0.5, 1.0, 4.3, 7.6,
                              6.5, 5.9, 5.4, 4.9, 8.9,
                              8.1, 7.5, 5.9, 4.5, 1.2,
                              5.5, 5.4, 4.6, 9.0, 8.2
)

time_per_division <- 1e-6 * c(0.2e3, 20, 10, 2, 1,
                              1, 1, 1, 1, 0.5,
                              0.5, 0.5, 0.5, 0.5, 10,
                              2, 2, 2, 1, 1
)

phaseshift_time <- phaseshift_in_divisions * time_per_division
phaseshift <- phaseshift_time * f_exp * 2 * pi
phaseshift <- phaseshift * - (180 / pi)

# FITTING --------------------------------------------------
model <- nls(phaseshift ~ a * Arg(vierpol(r, w_exp, C)),
             start = list(a = 1, C = 1e-9))

phi_fit <- predict(model, list(w_exp = w))

c_fit <- coef(model)[2]
errors(c_fit) <- summary(model)$coefficients[2, 2]

cutoff_freq_fit <- 1 / (2 * pi * r * c_fit)
errors(cutoff_freq_fit) <- find_confint(cutoff_freq_fit, 18)

# PLOTTING -------------------------------------------------
plot(logf, phi,
     type = "l",
     xaxt = "n",
     yaxt = "n",
     xlab = expression(italic("f") ~ "[Hz]"),
     ylab = "Phase shift [degrees]",
     ylim = c(-90, 0),
     col = "#005d00",
)

minor.ticks.axis(1, 10, mn = 0, mx = 12)
axis(2, at = seq(-90, 0, 30), las = 1)

rug(x = c(seq(-90, 0, 30) + 10, seq(-90, 0, 30) + 20),
    ticksize = -0.01, side = 2
)

points(log10(f_exp), phaseshift,
       pch = 21,
       bg = "blue"
)


abline(v = log10(cutoff_freq_fit), lty = 3)
abline(h = -45, lty = 3)

lines(logf, phi_fit,
      col = "red"
)

legend("topright",
      legend = c("Theoretical curve", "Fitted curve", "Data points"),
      lty = c(1, 1, NA),
      lwd = c(2, 2, NA),
      pch = c(NA, NA, 21),
      col = c("#005d00", "red", "blue"),
      pt.bg = "blue",
      plot = TRUE,
      cex = 1,
      pt.cex = 1.5
)

cat("Cutoff Frequency [Hz]: ")
print(cutoff_freq_fit)

dev.copy2pdf(file = "lowpassfilter_phase.pdf", width = 8, height = 6)
dev