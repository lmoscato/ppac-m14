rm(list = ls())
library(errors)
options(error.notation = "plus-minus") 

#Parameters
plotcolours = c("#8400ff", "#4c00ff", "#0051ff", "#0077ff", "#00a2ff")
width = 6
height = 6
# Data that was manually collected

#Errors of various instruments times the assumed human error  
hum.err = 2
balance.error = 0.1e-3  *hum.err # in g
mL25.error = 0.04e-3    *hum.err # in L
mL1.error  = 0.01e-3    *hum.err 
mL2.error  = 0.015e-3   *hum.err 
mL3.error  = 0.015e-3   *hum.err 
mL4.error  = 0.015e-3   *hum.err 
mL5.error  = 0.015e-3   *hum.err

#Stock solution 1
M.rhodamineB = 479.02           # g / mol
m.stocksol1 =  11.6e-3          # g weighed in amount of Rhodamine B
errors(m.stocksol1) = balance.error    # g error of weighing
V.stocksol1 = 25e-3             # L Volume of first solving
errors(V.stocksol1) = mL25.error# L error of Volume
n.stocksol1 = m.stocksol1/M.rhodamineB  # mol amount of Rhodamine B in stock 1
c.stocksol1 = n.stocksol1/V.stocksol1   # mol /L conc. of stock solution 1

#Stock solution 2
V.transfer.1.2 = 1e-3           # L volume transferred from stock 1 to stock 2
errors(V.transfer.1.2) = mL1.error          # L error of 1 mL pipette
n.stocksol2 = c.stocksol1*V.transfer.1.2    # mol amount of Rhodamine B in stock 2 
V.stocksol2 = 25e-3                         # L Volume of first solving
errors(V.stocksol2) = mL25.error            # L error of Volume
c.stocksol2 = n.stocksol2/V.stocksol2       # mol / L conc. of stock 2

#Measurements 1-5
V.transfer.2.mes = c(1:5)*1e-3  # L transferred volume from stock 2 to measurement
errors(V.transfer.2.mes) = c(mL1.error, mL2.error, mL3.error, mL4.error, mL5.error)
V.mes = 25e-3                   # L volume of measurement solution
errors(V.mes) = mL25.error      # L error of Volume in measurements
n.mes = c.stocksol2*V.transfer.2.mes
c.mes = n.mes / V.mes           # mol / L concentration of measurement solutions
c.mes.formatted = format(c.mes*1e6, notation  = "plus-minus", digits = 1)
c.mes.formatted[5] = format(c.mes[5]*1e6, notation  = "plus-minus", digits = 2)
c.mes.formatted = paste(c.mes.formatted)
#File handling

files <- list.files(path="./UV/UV_spec/", pattern=".txt", all.files=TRUE, full.names=TRUE) #Read in file names 
max.abs.nm = c(1:5)
max.abs.A  = c(1:5)
n = 1 #counter for loop
plot(0, 0, xlim = c(375, 800), ylim = c(0, 1), col = "white",
    ylab = expression(A), 
    xlab = expression(lambda/nm)
) #create empty plot for UV spectra

#Reading in data from files
for(file in files){
    df = read.delim(file, header = FALSE, dec = ",", skip = 19)
    colnames(df) = c("nm", "A")
    max.abs.nm[n] = df$nm[which.max(df$A)] #Find point of maximum absorbance
    max.abs.A[n] = max(df$A)
    points(df$nm, df$A, type = "l", col = plotcolours[n]) #Print spectrum to plot
    #arrows(400, 0.4, 500, 0.4)
    arrows(max.abs.nm[n]+40, max.abs.A[n]+0.03, max.abs.nm[n]+5, max.abs.A[n], length = 0.1, code = 2, angle = 22)
    text(max.abs.nm[n]+95, max.abs.A[n]+0.03, c(c.mes.formatted[n], expression("                           "*mu*M)))
    n = n+1
}
dataframe = data.frame(drop_errors(c.mes), errors(c.mes), max.abs.A, check.rows = FALSE, check.names = TRUE, stringsAsFactors = default.stringsAsFactors())
colnames(dataframe) = c("Concentration", "Conc.error", "Absorbance")
write.table(dataframe, file = "./UV/concentration_and_absorbance.dat", sep = "\t", row.names = FALSE, col.names = TRUE)
dev.copy2pdf(file="./UV/abs_vs_c.pdf", width = width, height = height)
dev.copy2eps(file="./UV/abs_vs_c.eps", width = width, height = height)
Gerade = lmObj <- lm(dataframe$Absorbance ~ dataframe$Concentration)
print(summary(Gerade))