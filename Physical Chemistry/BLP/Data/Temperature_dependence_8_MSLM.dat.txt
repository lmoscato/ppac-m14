
Datei: Temperature_dependence_8_MSLM.dat
Fit function:  a + b * exp(-c * t)
t-Intervall: 18.000 ... 769.500 ms
Parameters (Standard deviation):
  a = 0.00442 (0.00018)
  b = 0.13272 (0.00061)
  c = 7.272e+00 (5.865e-02) s^-1
  tau = 1.375e-01 (1.109e-03) s
