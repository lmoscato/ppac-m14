
Datei: Solvent_debendence_sol5_1_MSLM.dat
Fit function:  a + b * exp(-c * t)
t-Intervall: 405.000 ... 26685.000 ms
Parameters (Standard deviation):
  a = -0.03238 (0.00047)
  b = 0.16864 (0.00089)
  c = 1.549e-01 (1.939e-03) s^-1
  tau = 6.456e+00 (8.083e-02) s
