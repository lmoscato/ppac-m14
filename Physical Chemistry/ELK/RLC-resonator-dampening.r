rm(list = ls())
source(file = "minorticks.r")
source("find_confint.r")
library(extrafont)
library(errors)
windows(width = 8, height = 6)
options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
options(errors.notation = "plus-minus", errors.digits = 2)
par(family = "CM Roman")

# VIERPOL FUNCTION------------------------------------------
vierpol <- function(w, r, l, c) {
    zr <- r
    zc <- (0 + 1i) / (-w * c)
    zl <- (0 + 1i) * (w * l)
    ze <- zr + 1 / (1 / zc + 1 / zl)
    za <- 1 / (1 / zc + 1 / zl)
    g <- za / ze
    g
}

# VARIABLES DECLARATION-------------------------------------
r <- 1.001e3
L <- 2.32e-6 * 30^2 # nolint
cap <- 104.4e-9
entrance_voltage <- 10
t  <- seq(0, 1, length = 10000)
f_res <- 9847
f_res <- f_res * 1e-3

# EXPERIMENTAL DATA-----------------------------------------

f_exp <- 504.349 # f in Hz

time_per_division <- 100 # in microseconds

max_meas <- 0.5 * c(2.6, 1.6, 1.0, 0.7)
t_meas_max <- 0.1 * c(0.2, 1.2, 2.2, 3.1)
min_meas <- 0.5 * c(-2, -1.2, -0.8, -0.5)
t_meas_min <- 0.1 * c(0.7, 1.7, 2.6, 3.6)

times_concatenate <- c(t_meas_max, t_meas_min)
maxmin_concatenate <- c(max_meas, min_meas)

# FITTING---------------------------------------------------

model_max <- lm(log(max_meas) ~ t_meas_max)
max_intercept <- coef(model_max)[1]
max_slope <- coef(model_max)[2]
errors(max_slope) <- summary(model_max)$coefficients[2, 2]

model_min <- lm(log(-min_meas) ~ t_meas_min)
min_intercept <- coef(model_min)[1]
min_slope <- coef(model_min)[2]
errors(min_slope) <- summary(model_min)$coefficients[2, 2]

model_sine <- lm(times_concatenate ~ c(1, 3, 5, 7, 2, 4, 6, 8))
sine_intercept <- coef(model_sine)[1]
sine_slope <- coef(model_sine)[2]

tau <- 1 / (-mean(c(max_slope, min_slope)))

# PLOTTING--------------------------------------------------

plot(
    times_concatenate, maxmin_concatenate,
    xlim = c(0, 1),
    ylim = c(-1.5, 1.5),
    pch = 21,
    bg = "red",
    cex = 1,
    xlab = "Time since pulse [ms]",
    ylab = "Amplitude [V]",
    las = 1
)

points(tau + 0.02, 1 / exp(1) * max(maxmin_concatenate),
    pch = 21,
    cex = 1.2,
    bg = "blue"
)

lines(t, exp(max_intercept + max_slope * t), lty = 2, col = "red")
lines(t, -exp(min_intercept + min_slope * t), lty = 2, col = "red")
lines(t, exp(mean(c(max_intercept, min_intercept)) + sum(c(max_slope, min_slope)) * t / 2) #nolint
        * sin(t * pi / sine_slope)
)

legend(
    "topright",
    legend = c("Fitted line", "Envelopes", "Data points",  "Tau value"),
    lwd = c(2, 2, NA, NA),
    lty = c(1, 2, NA, NA),
    pch = c(NA, NA, 21, 21),
    col = c("black", "red", "black", "black"),
    pt.bg = c(NA, NA, "red", "blue"),
    pt.cex = c(NA, NA, 1, 1.2)
)

cat("Decay time [ms]: ")
print(tau)

dev.copy2pdf(file = "RLC-resonator-dampening.pdf", width = 8, height = 6)