rm(list=ls())
cat("

Dampened Square-wave

")
#Rechteckwellen gedämpfte Oszillation
max_meas=0.5*c(2.6, 1.6, 1.0, 0.7)
t_meas_max=0.1*c(0.2, 1.2, 2.2, 3.1)
min_meas=0.5*c(-2, -1.2, -0.8, -0.5)
t_meas_min=0.1*c(0.7, 1.7, 2.6, 3.6)
#y~A*exp(-x) -> ln(y)~ln(A)-x 
model_max <- lm(log(max_meas)~t_meas_max)
print(summary(model_max))
model_min <- lm(log(-min_meas)~t_meas_min)
print(summary(model_min))
model_sine<-lm(c(t_meas_max, t_meas_min)~c(1,3,5,7,2,4,6,8)) #correction factor for time shifts
print(summary(model_sine))
plot(c(t_meas_max, t_meas_min),c(max_meas,min_meas),ylim=c(-1.5,1.5),xlim=c(0.015,0.42),ylab="Voltage output [V]", xlab="time difference [ms]")
points(seq(0,0.5,0.005),exp(coefficients(model_max)[1]+coefficients(model_max)[2]*seq(0,0.5,0.005)),type="l",lty=2)
points(seq(0,0.5,0.005),-exp(coefficients(model_min)[1]+coefficients(model_min)[2]*seq(0,0.5,0.005)),type="l",lty=2)
points(seq(0,0.5,0.001), exp((coefficients(model_max)[1]+coefficients(model_min)[1])/2 + (coefficients(model_max)[2]+coefficients(model_min)[2])*seq(0,0.5,0.001)/2)
        *sin(seq(0,0.5,0.001)*pi/coefficients(model_sine)[2]),type="l")
dev.copy2pdf(file="Plot_7_Squarewave_decay_meas_calc.pdf")
dev.off()
cat("

")
cat(c("tau_dampening=",-(coefficients(model_max)[2]+coefficients(model_min)[2])/2,"±",sqrt((summary(model_min)$coefficients[2,2])^2+(summary(model_max)$coefficients[2,2])^2)*qt(0.975, df=2)/sqrt(4),"ms^(-1)","
"))
