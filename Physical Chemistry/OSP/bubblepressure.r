rm(list = ls())
library(extrafont)
library(errors)
source("minorticks.r")
options(errors.notation = "plus-minus")
windows(width = 8, height = 6)
par(family = "CM Roman")


#GLOBALS----------------------------------
g <- 9.80652
x <- seq(0, 1, length = 1000)

#CALIBRATION-------------------------------
data_calib <- read.table("DATA_cal.txt")
sigma_ref <- 42.7
depths <- data_calib[, 4]
maxima <- data_calib[, 2]
r_k <- (2 * sigma_ref * (depths[2] - depths[1])) / (depths[2] * maxima[1] - depths[1] * maxima[2]) #nolint

#DMSO DATA------------------------------------
x_dmso <- c(0, 0.03, 0.09, 0.24, 0.39, 0.61, 0.75, 1)
x_dmso_ext <- c(0, x_dmso)

lit_data <- read.table("litdata_dmsowater.dat")
x_dmso_lit <- lit_data[, 1]
surftens_lit <- lit_data[, 2]
red_surftens_lit <- (max(surftens_lit) - surftens_lit) / (max(surftens_lit) - min(surftens_lit)) #nolint

data_dmso_h1 <- read.table("DATA_dmso_h1.txt")
data_dmso_h2 <- read.table("DATA_dmso_h2.txt")
dmso_h1 <- 0.300
dmso_h2 <- data_dmso_h2[, 4]
pmax_dmso_h1 <- data_dmso_h1[, 2]
pmax_dmso_h2 <- data_dmso_h2[, 2]

surftens_dmso_dropweight <- read.table("ospdata.dat", header = TRUE)
surftens_dmso_dropweight <- surftens_dmso_dropweight[, 4]

surftens_dmso <- r_k / 2 * (dmso_h2 * pmax_dmso_h1 - dmso_h1 * pmax_dmso_h2) / (dmso_h2 - dmso_h1) #nolint
surftens_dmso <- c(70.8, surftens_dmso)
density_dmso <- 1 / g * (pmax_dmso_h2 - pmax_dmso_h1) / (dmso_h2 - dmso_h1)

red_surftens_dmso <- (max(surftens_dmso) - surftens_dmso) / (max(surftens_dmso) - min(surftens_dmso)) #nolint
red_surftens_dmso <- c(0, red_surftens_dmso)

#TRITON DATA----------------------------------
triton_weight <- c(0.0141, 0.0303, 0.0243, 0.0530, 0.5025,
                     1.5064, 0.2488, 0.3839, 2.5159
)
triton_totals <- 10 + c(.0261, .0333, .0171, .0180, .0325, .1002, .0283, .0343, .0025) #nolint
triton_conc <- triton_weight / triton_totals * 2e-2
triton_log <- log10(triton_conc)

triton_data_h1 <- read.table("DATA_triton_h1.txt")
triton_data_h2 <- read.table("DATA_triton_h2.txt")
triton_h1 <- 0.300
triton_h2 <- triton_data_h2[, 4]
pmax_triton_h1 <- triton_data_h1[, 2]
pmax_triton_h2 <- triton_data_h2[, 2]

surftens_triton <- r_k / 2 * (triton_h2 * pmax_triton_h1 - triton_h1 * pmax_triton_h2) / (triton_h2 - triton_h1) #nolint

plot(triton_log, surftens_triton,
    xlab = expression(w[Tx] ~ "/" ~ g/g),
    ylab = expression("Surface Tension" ~ "/" ~ mN ~ m^-1),
    xaxt = "n",
    ylim = c(30, 80)
)
minor.ticks.axis(1, 10, mn = -5, mx = -2)

dev.copy2pdf(file = "triton_surftens.pdf", width = 8, height = 6)
dev.off()

#DMSO REDUCED SURF TENSION PLOT------------------------------

windows(width = 8, height = 6)
par(family = "CM Roman")

model_red_surftens <- lm(red_surftens_dmso ~ poly(x_dmso_ext, 2, raw = TRUE))
predicition_red_surftens <- predict(model_red_surftens, newdata = data.frame(x_dmso_ext = x)) #nolint

plot(x_dmso_ext, red_surftens_dmso,
    xlab = expression(italic(x)[DMSO]),
    ylab = "Reduced Surface Tension",
    las = 1
)

lines(x, predicition_red_surftens)

lines(x, x,
    col = "gray",
    lty = 2
)

points(
    x_dmso_lit, red_surftens_lit,
    pch = 22,
    bg = "black",
    cex = 0.5
)



legend("bottomright",
    legend = c("Ideal line", "Fitted curve", "Data points", "Literature Values"), #nolint
    lty = c(2, 1, NA, NA),
    pch = c(NA, NA, 21, 22),
    pt.bg = c(NA, NA, "white", "black"),
    col = c("grey", "black", "black", "black"),
    pt.cex = c(NA, NA, 1, 0.5)
)

dev.copy2pdf(file = "bubblepressure_dmso_reduced.pdf", width = 8, height = 6)
dev.off()

#DMSO SURF TENSION PLOT--------------------------------------

windows(width = 8, height = 6)
par(family = "CM Roman")

model_bubblepressure <- lm(surftens_dmso ~ poly(x_dmso, 2, raw = TRUE))
prediction_bubblepressure <- predict(model_bubblepressure, newdata = data.frame(x_dmso = x)) #nolint

y_surftens_dmso <- seq(max(surftens_dmso), min(surftens_dmso), length = 1000)

plot(x_dmso, surftens_dmso,
    xlab = expression(italic(x)[DMSO]),
    ylab = expression("Surface Tension" ~ "/" ~ mNm^-1),
    las = 1,
    pch = 21,
    ylim = c(40, 75)
)

lines(x, prediction_bubblepressure)

lines(x, y_surftens_dmso,
    col = "gray",
    lty = 2
)

points(x_dmso_lit, surftens_lit,
    pch = 22,
    bg = "black",
    cex = 0.5
)

legend("topright",
    legend = c("Ideal line", "Fitted curve", "Data points", "Literature values"), #nolint
    lty = c(2, 1, NA, NA),
    pch = c(NA, NA, 21, 22),
    pt.bg = c(NA, NA, "white", "black"),
    col = c("grey", "black", "black", "black"),
    pt.cex = c(NA, NA, 1, 0.5)
)

dev.copy2pdf(file = "bubblepressure_dmso.pdf", width = 8, height = 6)
dev.off()

#DMSO SURF TENSION PLOT COMPARISON---------------------------

windows(width = 8, height = 6)
par(family = "CM Roman")

plot(x_dmso, surftens_dmso,
    xlab = expression(italic(x)[DMSO]),
    ylab = expression("Surface Tension" ~ "/" ~ mN ~ m^-1),
    ylim = c(40, 70)
)
points(x_dmso, surftens_dmso_dropweight,
    pch = 22,
    bg = "black",
    cex = 0.5
)

model_bubblepressure <- lm(surftens_dmso ~ poly(x_dmso, 2, raw = TRUE))
prediction_bubblepressure <- predict(model_bubblepressure, newdata = data.frame(x_dmso = x)) #nolint
lines(x, prediction_bubblepressure,
    lty = 2
)

model_dropweight <- lm(surftens_dmso_dropweight ~ poly(x_dmso, 2, raw = TRUE)) #nolint
prediction_dropweight = predict(model_dropweight, newdata = data.frame(x_dmso = x)) #nolint
lines(x, prediction_dropweight)

legend("topright",
    legend = c("Bubble pressure method", "Drop Weight method"),
    lty = c(2, 1),
    pch = c(21, 22),
    pt.bg = c("white", "black"),
    pt.cex = c(1, 0.5)
)

dev.copy2pdf(file = "method_comparison.pdf", width = 8, height = 6)
dev.off()

# BUBBLE PRESSURE DENSITY PLOT----------------------------

windows(width = 8, height = 6)
par(family = "CM Roman")

y_density <- seq(0.998, density_dmso[7], length = 1000)

plot(x_dmso, c(0.998, density_dmso),
    xlab = expression(italic(x)[DMSO]),
    ylab = expression("Density" ~ "/" ~ g ~ cm^-3),
    ylim = c(0.9, 1.2)
)

lines(x, y_density,
    lty = 2,
    col = "gray"
)

legend("topright",
    legend = c("Data points", "Ideal line"),
    lty = c(NA, 2),
    col = c("black", "gray"),
    pch = c(21, NA)
)

dev.copy2pdf(file = "bubblepressure_density.pdf", width = 8, height = 6)
dev.off()