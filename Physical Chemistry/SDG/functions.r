antoine <- function(T, c) {
  return(10^(c[1] - c[2] / (T + 273.15 + c[3])))
}

wilson1 <- function(x1, L12, L21) {
  x2 <- 1 - x1
  lng <- -log(x1 + L12 * x2) + x2*(L12 / (x1 + L12 * x2) - L21 / (L21 * x1 + x2))
  return(exp(lng))
}

wilson2 <- function(x2, L12, L21) {
  x1 <- 1 - x2
  lng <- -log(x2 + L12 * x1) - x1*(L12 / (x1 + L12 * x2) - L21 / (x2 + L21 * x1))
  return(exp(lng))
}

raoult1 <- function(x1, PV, L12, L21) {
  return(x1 * wilson1(x1, L12, L21) * PV / P)
}

raoult2 <- function(x2, PV, L12, L21) {
  return(x2 * wilson2(x2, L21, L12) * PV / P)
}

rootfinding <- function(T, x1, L12, L21) {
  return(
    raoult1(x1, antoine(T, antoine_hexane), L12, L21)
    + raoult2(1 - x1, antoine(T, antoine_toluene), L12, L21)
    - 1
  )
}