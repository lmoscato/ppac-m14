# PPAC Code & reports collection

R codes for PPAC (4th Semester) Praktikum at ETH. This repository collects codes and reports written by Luca Moscato, Martina Saporito and Tiago Würtner.

## General remarks

This code was written during FS2022 and could be wrong. User discretion advised (Especially for IR omg).

## R Scripts

It is possible that many of the scripts here containt the instruction ``par(family = "CM Roman")``, this was done to change the font of the plots to the LaTeX font (Computer Modern) through the ``extrafont``; however, correct implementation of this package is tricky due to it being outdated. If the code does not work, de-comment the line relative to this package and the ``par()`` command.

Many scripts also use the package [errors](https://cran.r-project.org/web/packages/errors/index.html) to perform error propagation calculations automatically, please consult the documentation for mor details.

Some scripts may contain the following lines: 
```r
cur_dir <- getwd() #To get the current WD
if (!grepl("X", cur_dir, ignore.case = TRUE)) { # if WD incorrect
    setwd("./X") #set correct WD (path relative to repository)
}
```
where X is the name of the experiment (and therefore the folder in which the script is found). This lines are used to check whether the correct R working directory is set, and if not, update to the correct path. This could cause problems depending on how you downloaded the code, so check that the correct path is set and eventually remove these lines if you don't need them. 

## LaTeX Template

The LaTeX template used to generate the reports seen here can be found [here](https://www.overleaf.com/read/jtbjcwqgycvc#79283a). Note you can duplicate it through Overleaf in order to be able to edit it.

#### Important LaTeX packages
The aforementioned template makes use of several packages. Here a brief introduction to them (click on the name to display the official ctan page with documentation):

###### [siunitx](https://www.ctan.org/pkg/siunitx)
Used to handle SI units in a coherent manner across the document. The settings can be changed in ```main.tex``` under ```UNITS SETTINGS```. The most important functions:

- ```\si{unit}``` to write a unit
    -  For example: ```\si{\kilo\gram\metre\squared\per\second\squared}``` becomes $ \mathrm{kg \ m^2 \ s^{-2}}$
- ```\SI{number}{unit}``` to write a number followed by a unit. Can also handle uncertainties by using \pm inside:
    - For example: ```\SI{1.67e10 \pm 1.12e2}{\joule}``` gives $1.67 \times 10^{10} \pm 1.12 \times 10^2~\mathrm{J}$

Many more functions and settings are available. Consult the documentation for extensive information.

###### [biblatex](https://www.ctan.org/pkg/biblatex)
Used to correctly handle bibliography and citations (by using \cite{} in the document body). Needs an appropriate ``.bib`` file, as the one found in the template.

Consult the documentation for more details. 

###### [listings](https://www.ctan.org/pkg/listings)
Used to display codes in the appendix in a more readable way. Settings for the code appearance are found in ``main.tex`` under ``CODE APPEAREANCE SETTINGS``.

In order to include an R script, use the command \lstinputlisting{file.txt} (or file.R also works), as shown in the file ``appendice.tex``.

Consult the documentation for more details.
