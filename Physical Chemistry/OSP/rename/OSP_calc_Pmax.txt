rm(list=ls())
windows(width = 8, height = 6)
#  calculate mean of pmax values
# ===============================


#############################################################

# Dateiname/Pfad angeben:
filename <- "OSP (Labor)/triton-9-2.csv"

#############################################################
#############################################################

local_range <- 100  # Bereich an Datenpunkten ber den ein lokales Maximum bestimmt wird (nach links und nach rechts)
mydata <- read.csv(filename, sep=";", skip=15)
index <- mydata[,1]
p <- mydata[,3]    #Druck in Pa

plot(index, p, col="white", ylab="Pressure  / Pa")
lines(index, p)

title("Mit 2 clicks den Bereich im Plot auswaehlen:")
Range <- locator(2)


#find maxima:
  maxima <- c()
  index_of_maxima <- c()
  i <- local_range+Range$x[1] +1
  n <- 1
  while (i <= Range$x[2]) {
    near_maxima <- c( p[(i-local_range):(i-1)], p[(i+1):(i+local_range)] )
    if(all(near_maxima <= p[i])){
      maxima[n]  <- p[i]
      index_of_maxima[n] <- i
      n <- n+1
      i <- i+local_range-1
    }
    i <- i+1
  }


plot(index, p, col="white", ylab="Pressure  / Pa")
lines(index, p)
points(index_of_maxima, maxima, col="red")

title("Sind die ausgewaehlten Maxima (rot) in Ordnung? \n Ja, ich will die Resultate speichern: 1 \n Nein, Resultate nicht speichern: 2")


if(readLines(n = 1) == 1){
  M <- data.frame(filename, round(mean(maxima), digits=2), round(sd(maxima), digits=2) )
  write.table(M, file="DATA.txt", col.names=FALSE, row.names=FALSE, append=TRUE)
  dev.off()
}