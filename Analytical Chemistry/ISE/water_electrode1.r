rm(list = ls()) #removing all variables
library(extrafont) #package for CM font

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (ppac-m14/ISE)
cur_dir <- getwd()
if (!grepl("ISE", cur_dir, ignore.case = TRUE)) {
    setwd("./ISE")
}

x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman") #these lines at every new plot!!

# data collection similar to calib curve

calibration <- read.table("Data/calibration_prediction_curve_elec1.txt")
xaxis <- seq(-5, -0.5, length = 1000) #xaxis for later approximations

#TICINO DATA------------------------------------

ticino1 <- read.table("Data/4-11-2022_16.16.49.txt", skip = 1)
ticino2 <- read.table("Data/4-11-2022_16.30.11.txt", skip = 1)
ticino3 <- read.table("Data/4-11-2022_16.44.58.txt", skip = 1)

#GRISONS DATA-----------------------------------

grisons1 <- read.table("Data/4-11-2022_16.59.49.txt", skip = 1)
grisons2 <- read.table("Data/4-11-2022_17.12.1.txt", skip = 1)
grisons3 <- read.table("Data/4-11-2022_17.23.8.txt", skip = 1)

# TICINO1 DATA COLLECTION---------------------------

time <- ticino1[, 1] #collecting data from .txt
potential <- ticino1[, 2]

plot(time, potential, #plotting pot. vs time
    type = "l",
    xlab = "Time [s]",
    ylab = "U [mV]"
)

locs <- locator(2) #awaiting user's input for range
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x, #plotting lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #mean of pot. in range
se_potential <- sd(potential[range]) #std. err. of pot. in range

potentials_ticino <- potential_mean
standard_deviations_ticino <- se_potential

dev.copy2pdf(file = "raw_data_plots/ticino1.pdf", width = 8, height = 6)
dev.off()

#TICINO2 DATA COLLECTION---------------------------------------

x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman") #these lines at every new plot!!

time <- ticino2[, 1] #collecting data from .txt
potential <- ticino2[, 2]

plot(time, potential, #plotting pot. vs time
    type = "l",
    xlab = "Time [s]",
    ylab = "U [mV]"
)

locs <- locator(2) #awaiting user's input for range
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x, #plotting lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #mean of pot. in range
se_potential <- sd(potential[range]) #std. err. of pot. in range

potentials_ticino <- c(potentials_ticino, potential_mean)
standard_deviations_ticino <- c(standard_deviations_ticino, se_potential)

dev.copy2pdf(file = "raw_data_plots/ticino2.pdf", width = 8, height = 6)
dev.off()

#TICINO3 DATA COLLECTION----------------------------------------------

x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman") #these lines at every new plot!!

time <- ticino3[, 1] #collecting data from .txt
potential <- ticino3[, 2]

plot(time, potential, #plotting pot. vs time
    type = "l",
    xlab = "Time [s]",
    ylab = "U [mV]"
)

locs <- locator(2) #awaiting user's input for range
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x, #plotting lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #mean of pot. in range
se_potential <- sd(potential[range]) #std. err. of pot. in range

potentials_ticino <- c(potentials_ticino, potential_mean)
standard_deviations_ticino <- c(standard_deviations_ticino, se_potential)

dev.copy2pdf(file = "raw_data_plots/ticino3.pdf", width = 8, height = 6)
dev.off()

#GRISONS1 DATA COLLECTION---------------------------------------------

x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman") #these lines at every new plot!!

time <- grisons1[, 1] #collecting data from .txt
potential <- grisons1[, 2]

plot(time, potential, #plotting pot. vs time
    type = "l",
    xlab = "Time [s]",
    ylab = "U [mV]"
)

locs <- locator(2) #awaiting user's input for range
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x, #plotting lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #mean of pot. in range
se_potential <- sd(potential[range]) #std. err. of pot. in range

potentials_grisons <- potential_mean
standard_deviations_grisons <- se_potential

dev.copy2pdf(file = "raw_data_plots/grisons1.pdf", width = 8, height = 6)
dev.off()

#GRISONS2 DATA COLLECTION---------------------------------------

x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman") #these lines at every new plot!!

time <- grisons2[, 1] #collecting data from .txt
potential <- grisons2[, 2]

plot(time, potential, #plotting pot. vs time
    type = "l",
    xlab = "Time [s]",
    ylab = "U [mV]"
)

locs <- locator(2) #awaiting user's input for range
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x, #plotting lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #mean of pot. in range
se_potential <- sd(potential[range]) #std. err. of pot. in range

potentials_grisons <- c(potentials_grisons, potential_mean)
standard_deviations_grisons <- c(standard_deviations_grisons, se_potential)

dev.copy2pdf(file = "raw_data_plots/grisons2.pdf", width = 8, height = 6)
dev.off()

#GRISONS3 DATA COLLECTION----------------------------------------------

x11(width = 8, height = 6) #UNIX LINE #nolint
par(family = "CM Roman") #these lines at every new plot!!

time <- grisons3[, 1] #collecting data from .txt
potential <- grisons3[, 2]

plot(time, potential, #plotting pot. vs time
    type = "l",
    xlab = "Time [s]",
    ylab = "U [mV]"
)

locs <- locator(2) #awaiting user's input for range
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x, #plotting lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #mean of pot. in range
se_potential <- sd(potential[range]) #std. err. of pot. in range

potentials_grisons <- c(potentials_grisons, potential_mean)
standard_deviations_grisons <- c(standard_deviations_grisons, se_potential)

dev.copy2pdf(file = "raw_data_plots/grisons3.pdf", width = 8, height = 6)
dev.off()


#TICINO CALCULATIONS-------------------------------------------------

# Here calculating the concentration of ticino based on the calibration curve.
approx_ticino_conc <- approx(calibration[, 1], xaxis, potentials_ticino, rule = 2)$y

ticino_concentrations <- 10^approx_ticino_conc * 10 # *10 bcs of dilution
ticino_concentration <- mean(ticino_concentrations) # mean of the three

se_ticino <- sd(ticino_concentrations) #std error of the three
confint_ticino <- se_ticino * qt(0.975, df = 2) / sqrt(3) #95% confint




#GRISONS CALCULATIONS------------------------------------------------

approx_grisons_conc <- approx(calibration[, 1], xaxis, potentials_grisons, rule = 2)$y #nolint

grisons_concentrations <- 10^approx_grisons_conc * 10
grison_concentration <- mean(grisons_concentrations)

se_grisons <- sd(grisons_concentrations)
confint_grisons <- se_grisons * qt(0.975, df = 2) / sqrt(3)


#PRINTING RESULTS-----------------------------------------------------------


data_for_plot <- data.frame(
    c(mean(approx_ticino_conc), mean(potentials_ticino)),
    c(mean(approx_grisons_conc), mean(potentials_grisons))
)

write.table(data_for_plot, file = "Data/electrode1_sample_data.txt", col.names = FALSE, row.names = FALSE)

cat(file = "Data/electrode1_water_results.txt",
    c(
        "ISE water results with first electrode \n",
        "---------------------------------------\n",
        "Ticino water Ca concentration: ", ticino_concentration, " pm ", confint_ticino, " mol/L", "\n", #nolint
        "Grisons water Ca concentration: ", grison_concentration, " pm ", confint_grisons, " mol/L", "\n" #nolint
    )
)