OSP Tropfenvolumen-Tensiometer
------------------------------
File                         : DMSO-wasser.dat 
OSP dmso-wasser measurements 
2 Messungen:
1 1.278 57.4
1 1.278 57.4

Resultate (mit Standardabweichungen):
Kalibrationsfaktor [mm^3/mm] :   41.40840 (0.12537) 
Kapillarradius          [mm] :    2.99500 (0.01000) 
Dichte Umgebung    [mg/mm^3] :    0.00106 
Verschiebung            [mm] :    1.27800 (0.00000) 
Masse                   [mg] :   57.40000 (0.00000) 
Volumen               [mm^3] :   52.92015 (0.16008) 
Dichte             [mg/mm^3] :    1.08466 (0.00328) 
Parameter                    :    0.79772 (0.00278) 
Korrekturfaktor              :    0.60032 (0.00011) 
Oberflaechenspannung  [mN/m] :   49.77960 (0.15779) 
Monte Carlo Simulation mit 500000 Samples. 
Wed Apr 13 14:02:47 2022 
