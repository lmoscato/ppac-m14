rm(list = ls()) # hey let's do a calibration curveeee
library(extrafont)

cur_dir <- getwd()
if (!grepl("IC", cur_dir, ignore.case = TRUE)) {
    setwd("./IC")
}

#aaand find a way to actually import data since the IC software is from 1995
# (yes you guessed right, it's time to import data by hand)

#CALIB DATA AND OTHER STUFF--------------------------------------------------

water_density <- 997 # mg/ml @25C

# all masses in milligrams
stock_nacl <- 18.5
stock_na2so4 <- 112.3
stock_nano3 <- 11.1
stock_milliq <- 49433.4

vol_milliq_stock <- stock_milliq / water_density #in ml

cl_stock <- stock_nacl / (vol_milliq_stock * 1e-3) #mg/l (ppm)
so4_stock <- stock_na2so4 / (vol_milliq_stock * 1e-3)
no3_stock <- stock_nano3 / (vol_milliq_stock * 1e-3)

calib_milliq_masses <- c(48804, 48881.9, 48558.7, 47504.3) #in mg
calib_stock_volumes <- c(0.5, 1, 1.5, 2) #in ml

xaxis <- seq(0, 20, length = 2000) #for later
xaxis2 <- seq(2, 100, length = 2000)


#CL Data and fitting----------------------------------------------
cl_areas <- c(0.348, 0.670, 1.059, 1.466) #in microsiemens*min
cl_conc <- (calib_stock_volumes * cl_stock) / ((calib_milliq_masses / water_density))

cl_model <- lm(cl_areas ~ cl_conc)
cl_prediction <- predict(cl_model,
    interval = "confidence",
    newdata = data.frame(cl_conc = xaxis)
)

#SO4 2- Data and fitting
so4_areas <- c(1.543, 3.079, 4.981, 6.992)
so4_conc <- (calib_stock_volumes * so4_stock) / ((calib_milliq_masses / water_density))

so4_model <- lm(so4_areas ~ so4_conc)

so4_prediction <- predict(so4_model,
    interval = "confidence",
    newdata = data.frame(so4_conc = xaxis2)
)

#NaNO3 Data and fitting

no3_areas <- c(0.116, 0.225, 0.353, 0.483)
no3_conc <- (calib_stock_volumes * no3_stock) / (calib_milliq_masses / water_density)


no3_fit <- lm(no3_areas ~ no3_conc)
no3_prediction <- predict(no3_fit,
    interval = "confidence",
    newdata = data.frame(no3_conc = xaxis)
)

#DOMAT WATER CALCULATIONS------------------------

#cl time

domat_cl_peaks <- c(0.487, 0.461, 0.488)
domat_cl_approx <- approx(cl_prediction[, 1], xaxis, domat_cl_peaks, rule = 2)$y

domat_cl_se <- sd(domat_cl_approx)

domat_cl_peaks <- mean(domat_cl_peaks)
domat_cl_approx <- mean(domat_cl_approx)

domat_cl_confint <- domat_cl_se * qt(0.975, df = 2) / sqrt(3)


#so4 time

domat_so4_peaks <- c(3.784, 3.594, 3.811)
domat_so4_approx <- approx(so4_prediction[, 1], xaxis2, domat_so4_peaks, rule = 2)$y

domat_so4_se <- sd(domat_so4_approx)

domat_so4_peaks <- mean(domat_so4_peaks)
domat_so4_approx <- mean(domat_so4_approx)

domat_so4_confint <- qt(0.975, df = 2) * domat_so4_se / sqrt(3)

#no3 time

domat_no3_peaks <- c(0.433, 0.413, 0.436)
domat_no3_approx <- approx(no3_prediction[, 1], xaxis, domat_no3_peaks, rule = 2)$y

domat_no3_se <- sd(domat_no3_approx)

domat_no3_peaks <- mean(domat_no3_peaks)
domat_no3_approx <- mean(domat_no3_approx)

domat_no3_confint <- qt(0.975, df = 2) * domat_no3_se / sqrt(3)

#RUCOLA CALCULATIONS-----------------------

rucola_mass <- 305.6 #mg

milliq_rucola_first <- 29168.9 #rucola in water first time
first_rucola_conc <- rucola_mass / (milliq_rucola_first / water_density * 1e-3) #ppm

milliq_rucola_second <- 48936.2 #mg
rucola_conc <- (0.5 * first_rucola_conc) / (milliq_rucola_second / water_density)

dilution_factor <- (milliq_rucola_first / rucola_mass) * (milliq_rucola_second / 501.9)


#cl now

rucola_cl_peaks <- c(0.148, 0.146, 0.145)
rucola_cl_approx <- approx(cl_prediction[, 1], xaxis, rucola_cl_peaks, rule = 2)$y

rucola_cl_se <- sd(rucola_cl_approx)

rucola_cl_peaks <- mean(rucola_cl_peaks)
rucola_cl_approx <- mean(rucola_cl_approx)

rucola_cl_confint <- qt(0.975, df = 2) * rucola_cl_se / sqrt(3)

rucola_cl_conc <- rucola_cl_approx * dilution_factor
rucola_cl_confint <- rucola_cl_confint * dilution_factor

#so4 now

rucola_so4_peaks <- c(0.372, 0.363, 0.358)
rucola_so4_approx <- approx(so4_prediction[, 1], xaxis2, rucola_so4_peaks, rule = 2)$y

rucola_so4_se <- sd(rucola_so4_approx)

rucola_so4_peaks <- mean(rucola_so4_peaks)
rucola_so4_approx <- mean(rucola_so4_approx)

rucola_so4_confint <- qt(0.975, df = 2) * rucola_so4_se / sqrt(3)

rucola_so4_conc <- rucola_so4_approx * dilution_factor
rucola_so4_confint <- rucola_so4_confint * dilution_factor

#no3

rucola_no3_peaks <- c(0.568, 0.571, 0.572)
rucola_no3_approx <- approx(no3_prediction[, 1], xaxis, rucola_no3_peaks, rule = 2)$y

rucola_no3_se <- sd(rucola_no3_approx)

rucola_no3_peaks <- mean(rucola_no3_peaks)
rucola_no3_approx <- mean(rucola_no3_approx)

rucola_no3_confint <- qt(0.975, df = 2) * rucola_no3_se / sqrt(3)

rucola_no3_conc <- rucola_no3_approx * dilution_factor
rucola_no3_confint <- rucola_no3_confint * dilution_factor

#Cl- calib plot-------------------------------------------
x11(width = 8, height = 6)
par(family = "CM Roman")



plot(cl_conc, cl_areas,
    xlab = expression("Cl"^"-" * " concentration [ppm]"),
    ylab = expression("Peak integral" ~ "[" * u * S * " " * min * "]"),
    xlim = c(1.5, 16),
    ylim = c(0.05, 1.5)
)


lines(xaxis, cl_prediction[, 1])

lines(xaxis, cl_prediction[, 2],
    col = 2,
    lty = 2
)

lines(xaxis, cl_prediction[, 3],
    col = 2,
    lty = 2
)

points(domat_cl_approx, domat_cl_peaks,
    col = "red",
    bg = "red",
    pch = 22
)

points(rucola_cl_approx, rucola_cl_peaks,
    col = "orange",
    bg = "orange",
    pch = 23
)

dev.copy2pdf(file = "plots/cl_calib.pdf")
dev.off()

#so42- calib plot------------------------------------------
x11(width = 8, height = 6)
par(family = "CM Roman")



plot(so4_conc, so4_areas,
    xlab = expression("SO"[4]^"2-" * " concentration [ppm]"),
    ylab = expression("Peak integral" ~ "[" * u * S * " " * min * "]"),
    xlim = c(5, 100),
    ylim = c(0, 7)
)

lines(xaxis2, so4_prediction[, 1])

lines(xaxis2, so4_prediction[, 2],
    col = 2,
    lty = 2
)

lines(xaxis2, so4_prediction[, 3],
    col = 2,
    lty = 2
)

points(domat_so4_approx, domat_so4_peaks,
    col = "red",
    pch = 22,
    bg = "red"
)

points(rucola_so4_approx, rucola_so4_peaks,
    pch = 23,
    col = "orange",
    bg = "orange"
)

dev.copy2pdf(file = "plots/so4_calib.pdf")
dev.off()

# no3 calib plot -------------------------------
x11(width = 8, height = 6)
par(family = "CM Roman")


plot(no3_conc, no3_areas,
    xlab = expression("NO"[3]^"-" * " concentration [ppm]"),
    ylab = expression("Peak integral [uS min]"),
    xlim = c(1, 12),
    ylim = c(0.1, 0.6)
)


lines(xaxis, no3_prediction[, 1])

lines(xaxis, no3_prediction[, 2],
    col = 2,
    lty = 2
)

lines(xaxis, no3_prediction[, 3],
    col = 2,
    lty = 2
)

points(domat_no3_approx, domat_no3_peaks,
    col = "red",
    bg = "red",
    pch = 22
)

points(rucola_no3_approx, rucola_no3_peaks,
    col = "orange",
    bg = "orange",
    pch = 23
)

dev.copy2pdf(file = "plots/no3_calib.pdf")
dev.off()