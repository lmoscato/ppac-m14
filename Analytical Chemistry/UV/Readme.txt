Greetings!
Here are three files that do the R stuff that's relevant. 
SSM Parser.R takes an SSm file and turns it into nice .txt files for Latex.

UV-input-parser.R takes the UV Spectra with the bottom comments removed and turns them
to Concentration and Absorbance.dat which contains the cooked down information. It also produces
abs_vs_c.pdf and eps

UV-Statistics does the Statistical analysis of the output data and returns epsilon