# Plots folder
 Here are all the plots generated from IR data, divided in folders based on the technique used:
 - ATR
 - Liquid IR
 - Nujol suspension
 - KBr Disc
 - Gas IR
## What files are found in the folders?
Other than the plot, for each spectrum a .txt file with the same name can be found as well. This file containes just two columns of numbers, which correspond to the x and y coordinates of the minima (drawn in red in the plots) found in the spectrum.