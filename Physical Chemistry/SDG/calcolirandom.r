a <- coef(model)
a_se <- summary(model)$coefficients[, 2]
conf_int <- a_se * qt(0.975, df = 8)

errors(a) <- conf_int
