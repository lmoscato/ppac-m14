rm(list = ls()) #clear all variables
library(extrafont)
library(plotrix)

cur_dir <- getwd()
if (!grepl("IR", cur_dir, ignore.case = TRUE)) {
    setwd("./IR")
}

source("find_minima.r")

#IMPORTING DATA --------------------------

data10 <- read.csv("Data/Liquid_IR/1_10_1-propanol.csv", skip = 2)
data20 <- read.csv("Data/Liquid_IR/1_20_1-propanol.csv", skip = 2)
data50 <- read.csv("Data/Liquid_IR/1_50_1-propanol.csv", skip = 2)
data100 <- read.csv("Data/Liquid_IR/1_100_1-propanol.csv", skip = 2)

xdata <- data.frame(data10[, 1],
    data20[, 1],
    data50[, 1],
    data100[, 1]
)

ydata <- data.frame(data10[, 2],
    data20[, 2],
    data50[, 2],
    data100[, 2]
)

#PLOTTING DATA IN SAME PLOT FOR COMPARISON-----------------------

x11(width = 8, height = 6)
par(family = "CM Roman", mfrow = c(1, 3))

pr <- seq_len(3800)

plot(xdata[pr, 1], ydata[pr, 1],
    type = "l",
    # xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    xlab  = "",
    ylab = "Transmittance [%]",
    ylim = c(0, 105),
    col = 1,
    lwd = 2,
    xlim = rev(range(xdata[pr, 1]))
)

for (i in 2:4) {
    lines(xdata[pr, i], ydata[pr, i],
    type = "l",
    col = i,
    lwd = 2
    )
}

pr <- 3940:11000

plot(xdata[pr, 1], ydata[pr, 1],
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "",
    ylim = c(0, 105),
    col = 1,
    lwd = 2,
    xlim = rev(range(xdata[pr, 1])),
    yaxt = "n"
)

for (i in 2:4) {
    lines(xdata[pr, i], ydata[pr, i],
    type = "l",
    col = i,
    lwd = 2
    )
}

pr <- 11500:12760

plot(xdata[pr, 1], ydata[pr, 1],
    type = "l",
    xlab = "",
    ylab = "",
    ylim = c(0, 105),
    col = 1,
    lwd = 2,
    xlim = rev(range(xdata[pr, 1])),
    yaxt = "n"
)

for (i in 2:4) {
    lines(xdata[pr, i], ydata[pr, i],
    type = "l",
    col = i,
    lwd = 2
    )
}



dev.copy2pdf(file = "plots/Liquid_IR/1-propanol-conc.pdf")
dev.off()

# PLOTTING JUST 1:20 RATIO FOR PEAK DETERMINATION--------------

x11(width = 8, height = 6)
par(family = "CM Roman", mfrow = c(1, 3))

pr <- seq_len(3800)
plot(xdata[pr, 2], ydata[pr, 2],
    type = "l",
    xlab = "",
    ylab = "Transmittance [%]",
    ylim = c(0, 105),
    col = 1,
    lwd = 2,
    xlim = rev(range(xdata[pr, 2])),
)

#finding minima in every data set (for later as well)
minima10 <- find_minima(ydata[, 1], 180, 10)
minima20 <- find_minima(ydata[, 2], 180, 10)
minima50 <- find_minima(ydata[, 3], 180, 10)
minima100 <- find_minima(ydata[, 4], 180, 10)

minima20_df <- data.frame(xdata[minima20, 2], ydata[minima20, 2])
write.table(minima20_df,
    file = "plots/Liquid_IR/1_20_1propanol.txt",
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)

points(xdata[minima20, 2], ydata[minima20, 2],
    col = 2,
    lwd = 2
)

pr <- 3940:11000

plot(xdata[pr, 2], ydata[pr, 2],
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "",
    ylim = c(0, 105),
    col = 1,
    lwd = 2,
    xlim = rev(range(xdata[pr, 2])),
    yaxt = "n"
)


points(xdata[minima20, 2], ydata[minima20, 2],
    col = 2,
    lwd = 2
)

pr <- 11500:12760


plot(xdata[pr, 2], ydata[pr, 2],
    type = "l",
    xlab = "",
    ylab = "",
    ylim = c(0, 105),
    col = 1,
    lwd = 2,
    xlim = rev(range(xdata[pr, 2])),
    yaxt = "n"
)

points(xdata[minima20, 2], ydata[minima20, 2],
    col = 2,
    lwd = 2
)

dev.copy2pdf(file = "plots/Liquid_IR/1_20_1propanol.pdf")
dev.off()


# PLOTTING CONCENTRATION EFFECTS TO VERIFY LAMBERTBEER LAW---------------------

x11(width = 8, height = 6)
par(family = "CM Roman", mfrow = c(1, 1))

# declaring some variables for concentration calculation

chcl3_density <- 1.4892 #g/ml
propanol_density <- 0.804 #g/ml
chcl3_molmass <- 119.38 #g/mol
propanol_molmass <- 60.0952 #g/mol

chcl3_volumes <- rep(1, 4) # mL
propanol_volumes <- 1e-3 * c(10, 20, 50, 100) # mL

propanol_moles <- (propanol_volumes * propanol_density) / propanol_molmass

propanol_concentration <- propanol_moles / chcl3_volumes

first_minima_indeces <- c(minima100[1], minima50[1], minima20[1], minima10[1])
first_minima <- NULL

for (i in 1:4) {
    first_minima[i] <- ydata[first_minima_indeces[i], 5 - i]
}


#now transmittance plot

plot(propanol_concentration, first_minima,
    xlab = "1-propanol concentration [mol/L]",
    ylab = "Transmittance of first peak"
)

transmittance_fit <- nls(first_minima ~ a * exp(-b * propanol_concentration) + c,
    start = list(a = 80, b = 2000, c = 10)
)
xaxis <- seq(min(propanol_concentration), max(propanol_concentration), length = 1000)

prediction_t <- predict(transmittance_fit,
    newdata = data.frame(propanol_concentration = xaxis)
)

# OH MY GOD THE FIT WORKED NICE

lines(xaxis, prediction_t)
grid()

dev.copy2pdf(file = "plots/Liquid_IR/concentration_effect_trans.pdf")
dev.off()

# now absorbance linear plot

x11(width = 8, height = 6)
par(family = "CM Roman")

minima_absorbance <- 2 - log10(first_minima)

plot(propanol_concentration, minima_absorbance,
    xlab = "1-propanol concentration [mol/L]",
    ylab = "Absorbance of first peak"
)

absorbance_fit <- lm(minima_absorbance ~ propanol_concentration)

abline(absorbance_fit)
grid()

dev.copy2pdf(file = "plots/Liquid_IR/concentration_effect_abs.pdf")
dev.off()