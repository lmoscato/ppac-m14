rm(list = ls()) #removing all variables
library(extrafont) # package to change font

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (ppac-m14/ISE)
cur_dir <- getwd()
if (!grepl("ISE", cur_dir, ignore.case = TRUE)) {
    setwd("./ISE")
}

source("../minorticks.r")

# DATA-----------------------------------------
cacl2_molarmass <- cacl2_molarmass <- 147.02 # grams per mole

stock1_mass <- 0.7480 # g
stock2_mass <- 0.0714 # g

stock_volume <- 0.050 # L

stock1_mole <- stock1_mass / cacl2_molarmass # mol
stock2_mole <- stock2_mass / cacl2_molarmass # mol

stock1_conc <- stock1_mole / stock_volume # g per mole
stock2_conc <- stock2_mole / stock_volume # g per mole

calib6_conc <- stock2_conc * 10e-5
calib5_conc <- stock2_conc * 10e-4
calib4_conc <- stock2_conc * 10e-3
calib3_conc <- stock2_conc * 10e-2
calib2_conc <- stock1_conc * 10e-2

concentrations <- c(
    calib6_conc,
    calib5_conc,
    calib4_conc,
    calib3_conc,
    calib2_conc
)
conc_log <- log10(concentrations)

potentials <- -1 * c(219.6, 216.3, 189.9, 165.2, 135.4)
xaxis <- seq(-6.5, -0.5, length = 1000) #xaxis seq. for prediction

#FITTING---------------------------------------
range <- c(1, 3:5) #to exclude bad point from fit
conc_log_fit <- conc_log[range]

calib_fit <- lm(potentials[range] ~ conc_log_fit) #linear fit of data
calib_pred <- predict(calib_fit, # predict based on model with conf. interval
    interval = "confidence",
    newdata <- data.frame(conc_log_fit = xaxis)
)


# PLOTTING------------------------------------

x11(width = 8, height = 6)  #Open graphic dev.
par(family = "CM Roman")

plot(conc_log_fit, potentials[range],
    xlab = expression(c[Ca] ~ "[mol/L]"),
    ylab = "U [mV]",
    xaxt = "n"
)

minor.ticks.axis(1, 10, mn = -6, mx = -1)

points(conc_log[2], potentials[2],
    pch = 4,
    col = "red"
)

lines(xaxis, calib_pred[, 1])

lines(xaxis, calib_pred[, 2],
    lty = 2,
    col = "red"
)

lines(xaxis, calib_pred[, 3],
    lty = 2,
    col = "red"
)

#plotting measured data points for samples

sample_data <- read.table("Data/ext_cal_elec2_results.txt")
cols <- c("red", "green", "orange")

for(i in 1:3) {
    points(sample_data[1, i]-0.41, sample_data[2, i],
        pch = 20 + i, #change point shape
        col = cols[i], #change colors
        bg = cols[i]
    )
}


dev.copy2pdf(file = "plots/calibration_electrode2.pdf", width = 8, height = 6)
dev.off()

#WRITE DATA TO FILE-------------------------------------------

#Export calibration data to file in order to use it in another script to approximate values.
write.table(calib_pred,
    file = "Data/calibration_prediction_curve_elec2.txt",
    col.names = FALSE,
    row.names =  FALSE
)