rm(list = ls()) #Delete all variables

k_values <- c(2.453, 2.274,3.899) #values of k obtained by analyzing absorbance data
k_mean <- mean(k_values) #mean value from three measurements

conf_int <- qt(0.975, df=length(k_values)-1) * sd(k_values) * sqrt(length(k_values))

print(k_mean)