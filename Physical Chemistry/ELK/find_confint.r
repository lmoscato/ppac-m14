library(errors)
find_confint <- function(x, deg_freedom) {
    qt(0.975, df = deg_freedom) * errors(x) / sqrt(deg_freedom + 1)
}