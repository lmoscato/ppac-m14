rm(list = ls())
source(file = "minorticks.r")
 library(extrafont)
 library(errors)
 windows(width = 8, height = 6)
options(vsc.dev.args = list(width = 1000, height = 1000 * 6 / 8))
options(errors.notation = "plus-minus", errors.digits = 2)
par(family = "CM Roman")

# FUNCTIONS------------------------------------------
vierpol <- function(w, r, l, c) {
    zr <- r
    zc <- (0 + 1i) / (-w * c)
    zl <- (0 + 1i) * (w * l)
    ze <- zr + 1 / (1 / zc + 1 / zl)
    za <- 1 / (1 / zc + 1 / zl)
    g <- za / ze
    g
}

find_confint <- function(x, deg_freedom) {
    qt(0.975, df = deg_freedom) * errors(x) / sqrt(deg_freedom + 1)
}

# VARIABLES DECLARATION-------------------------------------
r <- 1.001e3
L <- 2.32e-6 * 30^2 # nolint
cap <- 104.4e-9
entrance_voltage <- 10
theoretical_resonance_freq <- 1 / (2 * pi * sqrt(L * cap))
theoretical_resonance_freq <- log10(theoretical_resonance_freq)

logf <- seq(1, 6, length = 1000)
f <- 10^logf
w <- 2 * pi * f
abs_g <- abs(vierpol(w, r, L, cap))

# EXPERIMENTAL DATA-----------------------------------------

f_exp <- 1e3 * c(1.0513, 2.04636, 3.49041, 4.37654, 8.44402,
                 9.2987, 10.0643, 10.70391, 11.27612, 12.55891,
                 13.51844, 14.29113, 20.25346, 28.65474, 34.4407,
                 42.25312, 7.18536, 8.15735, 6.58913, 10.31054)
w_exp <- 2 * pi * f_exp

exit_voltage_in_divisions <- c(6.4, 5.0, 4.0, 7.5, 8.0,
                               6.0, 6.0, 5.0, 4.4, 6.2,
                               5.0, 4.2, 5.0, 6.0, 4.8,
                               7.6, 4.0, 6.8, 7.8, 5.9)

voltage_divisions_values <- c(20e-3, 50e-3, 0.1, 0.1, 0.5,
                              1, 1, 1, 1, 0.5,
                              0.5, 0.5, 0.2, 0.1, 0.1,
                              50e-3, 0.5, 0.5, 0.2, 1)

exit_voltages <- exit_voltage_in_divisions * voltage_divisions_values

g_exp <- exit_voltages / entrance_voltage

# FITTING---------------------------------------------------

model <- nls(g_exp ~ a * abs(vierpol(w_exp, R, L, c)),
            start = list(a = 1, R = 1000, c = 100e-9),
            control = nls.control(maxiter = 1000)
            )
r_fit <- coef(model)[2]
c_fit <- coef(model)[3]

errors(r_fit) <- summary(model)$coefficients[2, 2]
errors(c_fit) <- summary(model)$coefficients[3, 2]

f_res_fit <- 1 / (2 * pi * sqrt(L * c_fit))
errors(f_res_fit) <- find_confint(f_res_fit, 17) #nolint
# r_std_error <- summary(model)$coefficients[2, 2] #nolint
# c_std_error <- summary(model)$coefficients[3, 2] #nolint



g_fit <- predict(model, list(w_exp = w))
g_fit_707 <- max(g_fit) / sqrt(2)

# PLOTTING--------------------------------------------------
plot(logf, g_fit,
    type = "l",
    col = "red",
    xaxt = "n",
    xlab = expression(italic("f") ~ "[Hz]"),
    ylab = expression("|G" * (italic("f")) * "|"),
    las = 1,
    xlim = c(2, 6),
    ylim = c(0, 1)
)

minor.ticks.axis(1, 6, mn = 0, mx = 8)

points(log10(f_exp), g_exp,
       pch = 21,
       bg = "blue",
    )

lines(logf, abs_g,
      col  = "#005d00"
)

# abline(v = theoretical_resonance_freq, lty = 3) nolint
abline(v = log10(f_res_fit), lty = 3, col = "red", lwd = 2)
abline(h = g_fit_707, lty = 3)

locs <- locator(2)
x1 <- locs$x[1]
x2 <- locs$x[2]
deltaf_db <- 10^x2 - 10^x1

q <- f_res_fit / deltaf_db
errors(q) <- find_confint(q, 17)

segments(x1, g_fit_707, x2, lty = 1, col = "purple", lwd = 2)

# text(5, g_fit_707 + 0.1, bquote(italic(f)[res] * "=" ~ .(f[which(g_fit == max(g_fit))]) ~ Hz)) # nolint
# text(5, g_fit_707 + 0.05, bquote(Delta * italic(f)["3dB"] ~ "=" ~ .(deltaf_db) ~ "Hz")) # nolint



legend("topright",
      legend = c("Theoretical curve", "Fitted curve", "Data points"),
      lty = c(1, 1, NA),
      lwd = c(2, 2, NA),
      pch = c(NA, NA, 21),
      col = c("#005d00", "red", "blue"),
      pt.bg = "blue",
      plot = TRUE,
      cex = 1,
      pt.cex = 1.5
     )

print(summary(model))
cat("f_res from fit [Hz]: ")
print(f_res_fit)
cat("Q of resonator: ")
print(q)

dev.copy2pdf(file = "RLC-resonator-amplitude.pdf", width = 8, height = 6)
