rm(list = ls())
filename = list.files(path="./HPLC/exp3/", pattern=".txt", all.files=TRUE, full.names=TRUE)
named.list <- lapply(filename, read.table, skip = 43)
data = seq(from = 1, to = 2*length(filename)+1, length=length(filename))
i = 1
CHOSON = c(1:8)
filename = filename[CHOSON]

SINGLES = TRUE
SUPERPOSITION = FALSE

for(file in filename){ #load all data into data
    data[i] = data.frame(read.table(file, skip = 43)[,1])  
    data[i+1] = data.frame(read.table(file, skip = 43)[,3])
    i = i+2
}

#data[8*2] = list(unlist(data[2*8])-460) 
#data[7*2] = list(unlist(data[2*7])-24) 
if(SUPERPOSITION){

x11(width = 7, height = 7)
plot(unlist(data[1]), unlist(data[2]),
xlab = "t",
ylab = "Absorbacne",
type = "l", ylim = c(0, 1500), xlim = c(5, 6))
print(length(unlist(data[1])))
j = 1
for(j in c(1:length(filename))){
    k = 2*j
    points(unlist(data[k-1]), unlist(data[k]), type = "l")
}
abline(h = 0)
}

if(SINGLES){
    basefile = substr(filename,1, nchar(filename)-4)
    out.filename = paste(basefile, ".pdf", sep="", collapse = NULL)
    j = 1
    for(j in c(1:length(out.filename))){
        k = 2*j
        x11(width=4, height=4.5)
        plot(unlist(data[k-1]), unlist(data[k]), type = "l",
            xlab = "t",
            ylab = "Absorbacne")
        
        abline(h = 0)

        dev.copy2pdf(file = out.filename[j])
        print(j)
    }
    #write.table(dataframe, file = filename, sep = " ", row.names = FALSE, col.names = TRUE)
    
}