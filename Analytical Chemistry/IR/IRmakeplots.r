rm(list = ls())
library(extrafont)
library(plotrix)

cur_dir <- getwd()
if (!grepl("IR", cur_dir, ignore.case = TRUE)) {
    setwd("./IR")
}

source("find_minima.r")

#KBr DISK VANILLIN-------------------------------

x11(width = 8, height = 6)
par(family = "CM Roman")

data <- read.table("Data/KBr/Vanillin_KBr_disk.csv", skip = 2, sep = ",")

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 100, 10) #wow this works

plot(x, y,
    xlim = c(3300, 500),
    ylim = c(0, 105),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]"
)
water_peak <- c(x[3320], y[3320]) #bad function does not recognize water peak for some reason

points(water_peak[1], water_peak[2],
    pch = 21,
    bg = "white",
    col = "red",
    cex = 0.7
)

points(x[minima], y[minima],
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = "red"
)
x_write <- c(water_peak[1], x[minima])
y_write <- c(water_peak[2], y[minima])

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

#write in file
write.table(file = "plots/KBr/Vanillin_in_disk.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)

dev.copy2pdf(file = "plots/KBr/Vanillin_in_disk.pdf", width = 8, height = 6)
dev.off()

#ATR 4-aminobenzoic acid------------------------------------

x11(width = 8, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/ATR/4-aminobenzoic_acid_ATR.csv", skip = 2)

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 100, 5)

gap.plot(x, y,
    gap.axis = "x",
    gap = c(1900, 2300),
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
    ylim = c(60, 100)
)
gap.plot(x[minima], y[minima], add = TRUE,
    gap = c(1900, 2300),
    gap.axis = "x",
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)

x_write <- round(x[minima])
y_write <- round(y[minima])

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/ATR/4-aminobenzoic_acid_ATR.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/ATR/4-aminobenzoic_acid_ATR.pdf", width = 8, height = 6)
dev.off()

# VANILLIN ATR SPECTRUM-----------------------------------------------------

x11(width = 8, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/ATR/Vaniline_ATR.csv", skip = 2)

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 100, 5)

gap.plot(x, y,
    gap.axis = "x",
    gap = c(1900, 2400),
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
    ylim = c(70, 100)
)

water_peak <- c(x[3347], y[3347])

gap.plot(c(water_peak[1], x[minima]), c(water_peak[2], y[minima]),
    add = TRUE,
    gap = c(1900, 2400),
    gap.axis = "x",
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)

x_write <- c(water_peak[1], x[minima])
y_write <- c(water_peak[2], y[minima])

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/ATR/Vaniline_ATR.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/ATR/Vaniline_ATR.pdf", width = 8, height = 6)
dev.off()

#POLYETHYLENE GLYCOL ATR-------------------------------------

x11(width = 8, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/ATR/Polyethylene_glycol_ATR.csv", skip = 2)

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 60, 5)

gap.plot(x, y,
    gap.axis = "x",
    gap = c(1900, 2400),
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
    ylim = c(40, 100)
)

gap.plot(x[minima], y[minima],
    add = TRUE,
    gap = c(1900, 2400),
    gap.axis = "x",
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)

x_write <- x[minima]
y_write <- y[minima]

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/ATR/Polyethylene_glycol_ATR.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/ATR/Polyethylene_glycol_ATR.pdf", width = 8, height = 6)
dev.off()

# TEFLON ATR----------------------------------------------------

x11(width = 8, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/ATR/Teflon_ATR.csv", skip = 2)

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 60, 5)

gap.plot(x, y,
    gap.axis = "x",
    gap = c(1900, 2400),
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
)

gap.plot(x[minima], y[minima],
    add = TRUE,
    gap = c(1900, 2400),
    gap.axis = "x",
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)

x_write <- x[minima]
y_write <- y[minima]

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/ATR/Teflon_ATR.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/ATR/Teflon_ATR.pdf", width = 8, height = 6)
dev.off()

# VANILLIN IN NUJOL---------------------------------------

x11(width = 9, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/Nujol/Vanilin_probe_2.csv", skip = 2, sep = ";")

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 130, 5)

gap.plot(x, y,
    gap.axis = "x",
    gap = c(1200, 1500, 2600, 3100),
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
    xtics = c(4000, 3500, 3100, 2500, 2000, 1600, 800, 500)
)

gap.plot(x[minima], y[minima],
    add = TRUE,
    gap = c(1200, 1500, 2500, 3000),
    gap.axis = "x",
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)

x_write <- x[minima]
y_write <- y[minima]

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/Nujol/Vanilin_probe_2.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/Nujol/Vanilin_probe_2.pdf", width = 8, height = 6)
dev.off()

# VANILLIN IN CHLOROFORM-----------------------------------

x11(width = 8, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/Liquid_IR/Vanillin_in_chloroform.csv", skip = 2, sep = ",")

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 60, 5)

gaps <- c(1200, 1500, 3000, 3100)

gap.plot(x, y,
    gap.axis = "x",
    gap = gaps,
    xlim = c(4000, 500),
    ylim = c(0, 100),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
    ytics = c(0, 20, 40, 60, 80, 100),
)


gap.plot(x[minima], y[minima],
    add = TRUE,
    gap = gaps,
    gap.axis = "x",
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2,
)
axis.break(1, gaps[1], breakcol = "white", style = "gap")
axis.break(1, gaps[3] - 300, breakcol = "white", style = "gap")
axis.break(1, gaps[1] - 40)
axis.break(1, gaps[3] - 340)

# points(x[minima], y[minima],
    # col = 2,
    # pch = 21
# )

x_write <- x[minima]
y_write <- y[minima]

random_indeces <- which((x_write < 1200 | x_write > 1500) & (x_write < 3000 | x_write > 3100))

x_write <- x_write[random_indeces]
y_write <- y_write[random_indeces]

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

# write.table(file = "plots/Liquid_IR/Vanillin_in_chloroform.txt",
#     minima_df,
#     sep = "\t\t",
#     row.names = FALSE,
#     col.names = FALSE
# )



dev.copy2pdf(file = "plots/Liquid_IR/Vanillin_in_chloroform.pdf", width = 9, height = 6)
dev.off()

# AIR PLOT------------------------------------

x11(width = 9, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/Gas IR/air.csv", skip = 2, sep = ",")

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 10, 10)

plot(x, y,
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
)
mp <- c(x[6565], y[6565])

points(c(mp[1], x[minima]), c(mp[2], y[minima]),
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)


x_write <- c(mp[1], x[minima])
y_write <- c(mp[2], y[minima])

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/Gas IR/air.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/Gas IR/air.pdf", width = 8, height = 6)
dev.off()

#CO2 PLOT------------------------------------------------------


x11(width = 9, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/Gas IR/CO2.csv", skip = 2, sep = ",")

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 50, 5)

plot(x, y,
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
)

points(x[minima], y[minima],
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)


x_write <- x[minima]
y_write <- y[minima]

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)

write.table(file = "plots/Gas IR/CO2.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/Gas IR/CO2.pdf", width = 8, height = 6)
dev.off()

#CHLOROFORM GAS----------------------------------------


x11(width = 9, height = 6)
par(family = "CM Roman")

data <- read.csv("Data/Gas IR/chloroform_gas.csv", skip = 2, sep = ",")

x <- data[, 1]
y <- data[, 2]

minima <- find_minima(y, 200, 5)

plot(x, y,
    xlim = rev(range(x)),
    type = "l",
    xlab = expression("Wavenumber" ~ "[" * cm^-1 * "]"),
    ylab = "Transmittance [%]",
)
mp <- c(x[3868], y[3868])

points(c(mp[1], x[minima]), c(mp[2], y[minima]),
    pch = 21,
    cex = 0.7,
    bg = "white",
    col = 2
)


x_write <- c(mp[1], x[minima])
y_write <- c(mp[2], y[minima])

x_write <- round(x_write) #round bcs of resolution of detector
y_write <- round(y_write)

minima_df <- data.frame(x_write, y_write)
write.table(file = "plots/Gas IR/chloroform_gas.txt",
    minima_df,
    sep = "\t\t",
    row.names = FALSE,
    col.names = FALSE
)



dev.copy2pdf(file = "plots/Gas IR/chloroform_gas.pdf", width = 8, height = 6)
dev.off()