rm(list = ls()) #clearing all variables
library(extrafont)

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (ppac-m14/ISE)
cur_dir <- getwd()
if (!grepl("/ISE", cur_dir, ignore.case = TRUE)) {
    setwd("./ISE")
}
source("../minorticks.r") #sourcing file for minor logarithmic ticks

x11(width = 8, height = 6)
par(family = "CM Roman")

#READING CALIBRATION DATA-----------------------------------------
calib6 <- read.table("Data/4-11-2022_14.44.58.txt", skip = 1)
calib5 <- read.table("Data/4-11-2022_15.8.15.txt", skip = 1)
calib4 <- read.table("Data/4-11-2022_15.30.46.txt", skip = 1)
calib3 <- read.table("Data/4-11-2022_15.51.58.txt", skip = 1)
calib2 <- read.table("Data/4-11-2022_16.2.55.txt", skip = 1)


#CONCENTRATIONS--------------------------------------------------
cacl2_molarmass <- 147.02 # grams per mole
stock1_mass <- 0.7425 # g
stock2_mass <- 0.7405 # g
stock1_mole <- stock1_mass / cacl2_molarmass # mol
stock2_mole <- stock2_mass / cacl2_molarmass # mol
stock_volume <- 0.050 # in liters
stock1_conc <- stock1_mole / stock_volume # g per mole
stock2_conc <- stock2_mole / stock_volume # g per mole

calib6_conc <- stock2_conc * 10e-6
calib5_conc <- stock2_conc * 10e-5
calib4_conc <- stock2_conc * 10e-4
calib3_conc <- stock1_conc * 10e-3
calib2_conc <- stock1_conc * 10e-2

concentrations <- c(#Storing concentrations for later
    calib6_conc,
    calib5_conc,
    calib4_conc,
    calib3_conc,
    calib2_conc
)

xaxis <- seq(-7, -0.5, length = 1000) #sequence for xaxis used later

#10e-6 DATA COLLECTION------------------------------------------
time <- calib6[, 1]
potential <- calib6[, 2]



plot(time, potential, #plot pot against time
    type = "l",
    las = 1,
    xlab = "Time [s]",
    ylab = "U [V]"
)

locs <- locator(2) #ask user for input with mouse
range <- which(time >= locs$x[1] & time <= locs$x[2]) #get array indices for locs range #nolint

abline(v = locs$x, #draw lines to delimit range
    lty = 2
)

potential_mean <- mean(potential[range]) #find mean of pot. in range
se_potential <- sd(potential[range]) #standard error of pot. in range

potentials <- potential_mean  #save data in vectors for later
standard_deviations <- se_potential

dev.copy2pdf(file = "raw_data_plots/calib6_raw.pdf", width = 8, height = 6) #save pdf with raw data #nolint
dev.off() # graphic device off

#10e-5 DATA COLLECTION-------------------------------------------
# Same thing as before


x11(width = 8, height = 6)
par(family = "CM Roman") #copy this at the beginning of every new plot

time <- calib5[, 1] #read data in same variables as before
potential <- calib5[, 2]

plot(time, potential, #plot pot against time
    type = "l",
    las = 1,
    xlab = "Time [s]",
    ylab = "U [V]"
)

locs <- locator(2) #Same as before
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x,
    lty = 2
)

potential_mean <- mean(potential[range])
se_potential <- sd(potential[range])


#Here it's different: concatenate potentials vector with new value to be added
potentials <- c(potentials, potential_mean)
standard_deviations <- c(standard_deviations, se_potential)

dev.copy2pdf(file = "raw_data_plots/calib5_raw.pdf", width = 8, height = 6)
dev.off()

#10e-4 DATA COLLECTION----------------------------------------------

x11(width = 8, height = 6)    #UNIX LINE (de-comment to use)
par(family = "CM Roman") #copy this at the beginning of every new plot

time <- calib4[, 1] #read data in same variables as before
potential <- calib4[, 2]

plot(time, potential, #plot pot against time
    type = "l",
    las = 1,
    xlab = "Time [s]",
    ylab = "U [V]"
)

locs <- locator(2) #Same as before
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x,
    lty = 2
)

potential_mean <- mean(potential[range])
se_potential <- sd(potential[range])

potentials <- c(potentials, potential_mean)
standard_deviations <- c(standard_deviations, se_potential)

dev.copy2pdf(file = "raw_data_plots/calib4_raw.pdf", width = 8, height = 6)
dev.off()

#10e-3 DATA COLLECTION----------------------------------------------

x11(width = 8, height = 6)    #UNIX LINE (de-comment to use) # nolint
par(family = "CM Roman") #copy this at the beginning of every new plot

time <- calib3[, 1] #read data in same variables as before
potential <- calib3[, 2]

plot(time, potential, #plot pot against time
    type = "l",
    las = 1,
    xlab = "Time [s]",
    ylab = "U [V]"
)

locs <- locator(2) #Same as before
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x,
    lty = 2
)

potential_mean <- mean(potential[range])
se_potential <- sd(potential[range])

potentials <- c(potentials, potential_mean)
standard_deviations <- c(standard_deviations, se_potential)

dev.copy2pdf(file = "raw_data_plots/calib3_raw.pdf", width = 8, height = 6)
dev.off()

#10e-2 DATA COLLECTION----------------------------------------------

x11(width = 8, height = 6)    #UNIX LINE (de-comment to use)
par(family = "CM Roman") #copy this at the beginning of every new plot

time <- calib2[, 1] #read data in same variables as before
potential <- calib2[, 2]

plot(time, potential, #plot pot against time
    type = "l",
    las = 1,
    xlab = "Time [s]",
    ylab = "U [V]"
)

locs <- locator(2) #Same as before
range <- which(time >= locs$x[1] & time <= locs$x[2])

abline(v = locs$x,
    lty = 2
)

potential_mean <- mean(potential[range])
se_potential <- sd(potential[range])

potentials <- c(potentials, potential_mean)
standard_deviations <- c(standard_deviations, se_potential)

dev.copy2pdf(file = "raw_data_plots/calib2_raw.pdf", width = 8, height = 6)
dev.off()

#DRAW CALIBRATION CURVE---------------------------------------------

x11(width = 8, height = 6) #UNIX LINE (de-comment to use)
par(family = "CM Roman") #copy this at the beginning of every new plot

conc_log <- log10(concentrations) #take log of concentrations

plot(conc_log, potentials, #plot conc log against pot.
    las = 1,
    xlab = expression("c"[Ca] ~ "[mol/L]"),
    ylab = "U [V]",
    xaxt = "n"
)

minor.ticks.axis(1, 10, mn = -6, mx = -2) #draw logarithmic axis

calibration_fit <- lm(potentials ~ conc_log) # fit data linearly
prediction_calib <- predict(calibration_fit, #predict with conf int
    interval = "confidence",
    newdata = data.frame(conc_log = xaxis)
)

lines(xaxis, prediction_calib[, 1]) #draw fit

lines(xaxis, prediction_calib[, 2], #draw lwr confint
    lty = 2,
    col = "red"
)

lines(xaxis, prediction_calib[, 3], #draw upr confint
    lty = 2,
    col = "red"
)

#plotting sample points

sample_data <- read.table("Data/electrode1_sample_data.txt")

cols <- c("red", "green")

for (i in 1:2) {
    points(
        sample_data[1, i], sample_data[2, i],
        pch = 20 + i,
        col = cols[i],
        bg = cols[i]
    )
}

dev.copy2pdf(file = "plots/calibration_electrode1.pdf", width = 8, height = 6)
dev.off()

#EXPORT DATA-------------------------------------------------

# exporting the calibration prediction (linear model) in a .txt file

write.table(prediction_calib,
    file = "Data/calibration_prediction_curve_elec1.txt",
    col.names = FALSE,
    row.names = FALSE
)