
# HEY YOU! THIS CODE IS CURSED, BUT IT WORKS (wow it's probably so inefficient)

find_minima <- function(x, threshold_ind, threshold_val) {
    if (length(x) == 1) {
        return(x)
    }
    indeces_of_minima <- NULL #declare var for storing indexes in x vector
    i <- 1 #counter to check all x values
    j <- 1 #counter in minima vector
    while (i <= length(x)) {
        if (i - threshold_ind <= 0) {
            range <- 1:(i + threshold_ind)
        }
        else if (i + threshold_ind >= length(x)) {
            range <- (i - threshold_ind):length(x)
        }
        else {
           range <- (i - threshold_ind):(i + threshold_ind)
        }

        if (all(x[range] >= x[i])) {
            depth <- diff(range(x[range]))
            if (depth >= threshold_val) {
                indeces_of_minima[j] <- i
                j <- j + 1
                # i <- max(range)
            }
        }
        i <- i + 1
    }
    indeces_of_minima
}