
Datei: Solvent_debendence_sol2_1_MSLM.dat
Fit function:  a + b * exp(-c * t)
t-Intervall: 180.000 ... 10788.000 ms
Parameters (Standard deviation):
  a = -0.00008 (0.00025)
  b = 0.30093 (0.00057)
  c = 4.238e-01 (1.740e-03) s^-1
  tau = 2.360e+00 (9.688e-03) s
