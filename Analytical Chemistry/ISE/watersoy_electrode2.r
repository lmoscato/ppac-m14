rm(list = ls()) #clearing workspace
library(extrafont) #package for CM font
library(errors)

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (ppac-m14/ISE)
cur_dir <- getwd()
if (!grepl("ISE", cur_dir, ignore.case = TRUE)) {
    setwd("./ISE")
}

source("../minorticks.r")

options(errors.notation = "plus-minus", errors.digits = 5)

# DATA--------------------------------

# Data from the electrode for ext. cal. method
ticino_potentials <- -0.1 * c(2190, 2180, 2177)
ticino_p_mean <- mean(ticino_potentials)
grisons_potentials <- -0.1 * c(2203, 2209, 2211)
grisons_p_mean <- mean(grisons_potentials)
soy_potentials <- -0.1 * c(1986, 1996, 1988)
soy_p_mean <- mean(soy_potentials)

# Data for standard addition method
calib2_conc <- 0.1017549 # mole per liter of added solution
slope_factor <- 20.58448 # for every order of magnitude from calibration fit
std_addition_potentials <- -0.1 * c(1729, 1647, 1596, 1570, 1543)
added_volume <- 1e-3 * c(2, 4, 6, 8, 10)
soy_volume <- 1e-3 * 0.5 #soy used at the beginning

#other variables needed
xaxis <- seq(-6, -0.5, length = 1000) # for later
xaxis2 <- seq(-1e-3, 1.5e-3, length = 1000)
calib_data <- read.table("Data/calibration_prediction_curve_elec2.txt")

#CALCULATIONS FOR EXT. CAL---------------------------------

#Ticino calculations
ticino_concentrations_log <- approx(calib_data[, 1], xaxis, ticino_potentials, rule = 2)$y
ticino_mean_log <- mean(ticino_concentrations_log)
ticino_concentrations <- 10^ticino_concentrations_log # convert back from log scale
ticino_mean <- mean(ticino_concentrations)
ticino_conf <- sd(ticino_concentrations) * qt(0.975, df = 2) / sqrt(3)

#Grisons calculations
grisons_concentrations_log <- approx(calib_data[, 1], xaxis, grisons_potentials, rule = 2)$y
grisons_mean_log <- mean(grisons_concentrations_log)
grisons_concentrations <- 10^grisons_concentrations_log #convert back from log scale
grisons_mean <- mean(grisons_concentrations)
grisons_conf <- sd(grisons_concentrations) * qt(0.975, df = 2) / sqrt(3)

#Soy calculations
soy_concentrations_log <- approx(calib_data[, 1], xaxis, soy_potentials, rule = 2)$y
soy_mean_log <- mean(soy_concentrations_log)
soy_concentrations <- 10^soy_concentrations_log #convert back from log scale
soy_mean <- mean(soy_concentrations)
soy_conf <- sd(soy_concentrations) * qt(0.975, df = 2) / sqrt(3)

#write results in file to plot them in cal. plot.

ext_cal_results <- data.frame(
    c(ticino_mean_log, ticino_p_mean),
    c(grisons_mean_log, grisons_p_mean),
    c(soy_mean_log, soy_p_mean)
)

write.table(ext_cal_results, file = "Data/ext_cal_elec2_results.txt", row.names = FALSE, col.names = FALSE)

#concentrations of 1:99 solutions found, now x100 to find real one

#Ticino
ticino_mean <- ticino_mean * 100 #bcs of dilution
ticino_conf <- ticino_conf * 100

#Grisons
grisons_mean <- grisons_mean * 100
grisons_conf <- grisons_conf * 100

#Soy
soy_mean <- soy_mean * 100
soy_conf <- soy_conf * 100

# SAVING RESULTS IN  A FILE-----------------------------------------------------------
cat(file = "Data/electrode2_extcal_results.txt",
    c(
        "ISE second electrode external calibration results \n",
        "---------------------------------------\n",
        "Ticino water Ca concentration: ", ticino_mean, " pm ", ticino_conf, " mol/L", "\n",
        "Grisons water Ca concentration: ", grisons_mean, " pm ", grisons_conf, " mol/L", "\n",
        "Soy sauce Ca concentration: ", soy_mean, " pm ", soy_conf, " mol/L \n"
    )
)

# CALCULATIONS FOR STANDARD ADDITION--------------------------------------------------

moles_added <- calib2_conc * added_volume #xaxis
delta_e <- diff(std_addition_potentials)
delta_e <- c(delta_e)
yaxis_data <- (soy_volume + added_volume) * 10^ (delta_e / slope_factor) #yaxis data

range <- seq_len(length(moles_added) - 1) #good values (last one is bad)
moles_added_fit <- moles_added[range]

#linear model now
std_addition_fit <- lm(yaxis_data[range] ~ moles_added_fit)
std_addition_pred <- predict(std_addition_fit,
    interval = "confidence",
    newdata = data.frame(moles_added_fit = xaxis2)
)
coefs <- coef(std_addition_fit)
errors(coefs) <- summary(std_addition_fit)$coefficients[, 2]

x_intercept <- -coefs[1] / coefs[2]
soy_conc_std_addition <- -x_intercept / soy_volume
errors(soy_conc_std_addition) <- errors(soy_conc_std_addition) * qt(0.975, df = 2) / sqrt(4)

x11(width = 8, height = 6)
par(family = "CM Roman")

plot(moles_added_fit, yaxis_data[range],
    # ylim = c(0, max(yaxis_data)),
    ylim = c(-0.02, 0.03),
    xlim = c(-5.5e-4, 1.1e-3),
    ylab = expression((V[u] + V[s]) * 10^(delta * E/S)),
    xlab = expression(C[s] * V[s] ~ "[mol]")
)

ltys <- c(1, 2, 2)
cols <- c("black", "red", "red")

for (i in 1:3) {
    lines(xaxis2, std_addition_pred[, i],
        lty = ltys[i],
        col = cols[i]
    )
}

abline(h = 0, lty = 3)
abline(v = 0, lty = 3)

points(moles_added[5], yaxis_data[5],
    pch = 4,
    col = "red"
)

points(x_intercept, 0,
    col = "orange",
    bg = "orange",
    pch = 21
)

dev.copy2pdf(width = 8, height = 6, file = "plots/std_addition_plot.pdf")
dev.off()

#PRINTING RESULTS TO A FILE---------------------------------------------

cat(file = "Data/electrode2_stdadd_results.txt",
    c(
        "ISE second electrode standard addition results \n",
        "---------------------------------------\n",
        "Soy sauce Ca concentration: ", soy_conc_std_addition, " pm ", errors(soy_conc_std_addition), "mol/L \n"
    )
)